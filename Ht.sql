USE [foodcourt]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Agent]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Agent](
	[AgentID] [int] IDENTITY(1,1) NOT NULL,
	[AgentName] [varchar](50) NOT NULL,
	[AgentPhone] [varchar](50) NOT NULL,
	[AgentEmailID] [varchar](200) NOT NULL,
	[AgentPercentage] [char](5) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Agent] PRIMARY KEY CLUSTERED 
(
	[AgentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Category]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[SectionID] [int] NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Cuisine]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Cuisine](
	[CuisineID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[CuisineType] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Cuisine] PRIMARY KEY CLUSTERED 
(
	[CuisineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[ZIPCode] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Item]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Item](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[ItemName] [varchar](50) NOT NULL,
	[ItemDescription] [varchar](4000) NOT NULL,
	[ItemImage] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Item] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ItemDetails]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ItemDetails](
	[ItemDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemCode] [varchar](50) NOT NULL,
	[ItemSize] [varchar](50) NOT NULL,
	[SalesPrice] [varchar](50) NOT NULL,
	[TaxPrice] [varchar](50) NOT NULL,
	[Discount] [varchar](50) NOT NULL,
	[UOM] [varchar](50) NOT NULL,
	[CostPrice] [varchar](50) NOT NULL,
	[MBQ] [varchar](50) NOT NULL,
	[StackUOM] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_ItemDetails] PRIMARY KEY CLUSTERED 
(
	[ItemDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Lookup]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Lookup](
	[LookupID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryID] [int] NOT NULL,
	[LookupName] [varchar](50) NOT NULL,
	[LookupCode] [char](2) NOT NULL,
	[LookupDescription] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Lookup] PRIMARY KEY CLUSTERED 
(
	[LookupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_LookupCategory]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_LookupCategory](
	[LookupCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryName] [varchar](50) NOT NULL,
	[LookupCategoryCode] [char](2) NOT NULL,
	[LookupCategoryDescription] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_LookupCategory] PRIMARY KEY CLUSTERED 
(
	[LookupCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Order]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Order](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[lk_OrderStatus] [int] NOT NULL,
	[lk_OrderType] [int] NOT NULL,
	[OrderAmount] [varchar](200) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Order] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_OrderDetails]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OrderDetails](
	[OrderDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[ItemDetailsID] [int] NOT NULL,
	[ItemQuantity] [int] NOT NULL,
	[ItemOrderNotes] [varchar](200) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[OrderDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Restaurant]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Restaurant](
	[RestaurantID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantName] [varchar](200) NOT NULL,
	[RestaurantShortName] [varchar](10) NOT NULL,
	[RestaurantDescription] [varchar](500) NOT NULL,
	[TAXID] [varchar](200) NOT NULL,
	[PrimaryContactName] [varchar](50) NULL,
	[PrimaryContactEmail] [varchar](50) NULL,
	[PrimaryContactNumber] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Restaurant] PRIMARY KEY CLUSTERED 
(
	[RestaurantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_RestaurantDetails]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RestaurantDetails](
	[RestaurantDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[Address1] [varchar](200) NOT NULL,
	[Address2] [varchar](200) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[ZIPCode] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_RestaurantDetails] PRIMARY KEY CLUSTERED 
(
	[RestaurantDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_RestaurantImage]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RestaurantImage](
	[RestaurantImageID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[RestaurantImage] [varchar](500) NOT NULL,
 CONSTRAINT [PK_tbl_RestaurantImage] PRIMARY KEY CLUSTERED 
(
	[RestaurantImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_RestaurantTimings]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RestaurantTimings](
	[RestaurantTimingsID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[Day] [varchar](50) NOT NULL,
	[StartTime] [varchar](50) NOT NULL,
	[EndTime] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_RestaurantTimings] PRIMARY KEY CLUSTERED 
(
	[RestaurantTimingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Section]    Script Date: 2015-12-05 17:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Section](
	[SectionID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[SectionName] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Section] PRIMARY KEY CLUSTERED 
(
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201509021506085_InitialCreate', N'takeaway.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CDB6EDC36107D2FD07F10F4D416CECA9726488DDD04CEDA6E8DC617649DA06F0157E2AE8548942A518E8DA25FD6877E527FA14389BAF1A2CBAEBCBB0E020416393C331C0EC9E170B8FFFDF3EFF8ED83EF19F7388ADD804CCC83D1BE696062078E4B961333A18B17AFCDB76FBEFF6E7CE6F80FC6A79CEE88D1414B124FCC3B4AC363CB8AED3BECA378E4BB7614C4C1828EECC0B790135887FBFBBF58070716060813B00C63FC2121D4F571FA019FD380D838A409F22E03077B312F879A598A6A5C211FC721B2F1C4A4E80B465FD1E32823358D13CF4520C60C7B0BD3408404145110F2F8638C67340AC872164201F26E1F430C740BE4C5980B7F5C9277EDC7FE21EB875536CCA1EC24A681DF13F0E0882BC6129BAFA45EB3501CA8EE0C544C1F59AF53F54DCC0B07A7451F020F1420323C9E7A11239E9897058B9338BCC27494371C6590E711C07D0DA22FA32AE29ED1B9DD5E614887A37DF66FCF98261E4D223C2138A111F2F68C9B64EEB9F6EFF8F136F882C9E4E860BE387AFDF215728E5EFD8C8F5E567B0A7D05BA5A0114DD44418823900D2F8AFE9B86556F67890D8B6695369956C096604E98C6257A788FC992DEC16C397C6D1AE7EE0376F2126E5C1F890B5388196B94C0E755E27968EEE1A2DE6AE4C9FE6FE07AF8F2D5205CAFD0BDBB4C875EE00F13278279F5017B696D7CE786D9F4AA8DF7674E761E053EFBAEDB5756FB79162491CD3A1368496E51B4C4B42EDDD82A8DB7934933A8E1CD3A47DD7DD36692CAE6AD24651D5A6526E42C363D1B72799F966F678B3B094318BCD4B498469A0C4ED8A94642D33D2327288DE6A0ABD110E8CCB7BC069EF9C8F50658043B7001E763E1463E2E7AF92E009343A4B7CC37288E610D707E43F15D83E8F0E700A2CFB09D44609A338AFCF0C9B9DDDC05045F25FE9C59FCE6780D3634B75F837364D3203A23ACD5DA78EF03FB4B90D033E29C228A3F523B07649FB7AEDF1D6010714E6C1BC7F139183376A601F8D639E005A14787BDE1D8EAB46D2764EA21D7577B21C23AFA39272D3D113585E48D68C8541E4993A8EF83A54BBA899A93EA45CD285A45E5647D456560DD24E5947A415382563933AAC17CBC74848677F252D8DDF7F2D6DBBC756B41458D335821F1AF98E0089631E706518A23528E409775631BCE423A7C8CE993EF4D29A74FC84B8666B5D26C481781E167430ABBFBB32115138AEF5D8779251D8E3E3931C077A2579FAADAE79C20D9A6A743AD9B9B66BE993540375D4EE238B0DD741628825E3C6451971F7C38A33D7E91F5468C8140C7C0D05DB6E54109F4CD148DEA9A9C620F536C9CD85950708A621B39B21AA1434E0FC1F21D552158190BA90BF793C4132C1D47AC116287A01866AA4BA83C2D5C62BB21F25AB524B4ECB885B1BE173CC49A531C62C218B66AA20B7375E8830950F01106A54D4363AB6271CD86A8F15A7563DEE6C296E32E45243662932DBEB3C62EB9FFF62486D9ACB10D1867B34ABA08A00DE36DC340F959A5AB018807975D3350E1C4A43150EE526DC440EB1ADB8281D655F2EC0C343BA2761D7FE1BCBA6BE6593F286F7E5B6F54D7166CB3A68F1D33CDCCF78436145AE04836CFD339ABC40F5471380339F9F92CE6AEAE68220C7C86693D6453FABB4A3FD46A06118DA809B034B416507E0128014913AA8770792CAF513AEE45F480CDE36E8DB07CED17602B362063572F422B84FAEB52D1383B9D3E8A9E15D6201979A7C34205476110E2E255EF7807A5E8E2B2B262BAF8C27DBCE14AC7F8603428A8C573D52829EFCCE05ACA4DB35D4B2A87AC8F4BB6969604F749A3A5BC33836B89DB68BB92144E410FB7602D15D5B7F081265B1EE928769BA26E6C65C951BC606C69B2A8C697280C5DB2AC6455F1126396A5544D5FCCFAA71BF9198665C78AACA342DA82130D22B4C4422DB00649CFDD28A6A788A23962719EA9E34B64CABD55B3FCE72CABDBA73C88F93E9053B3BFB316E2B57D6DA3953D110E700EDDF3993B93C6D01583AF6E6EB01437E4A14811B69F065EE213BD77A56F9D5DDE55DB672532C2D812E497BC274955928F5BD77BA7519167C4102354782EAB8F921E42A7EBDCEFAC6A5BE78BEA51F2D054154517AEDADAA8E95C98EE2325BA86FD07AA15E1696614CF47A902F0A29E1895940609AC52D71DB59E7552C5ACD7744714524BAA9042550F29AB09243521AB152BE16934AAA6E8CE414E19A9A2CBB5DD9115C923556845F50AD80A99C5BAEEA88AFC922AB0A2BA3B76996C22AEA03BBC67690F2CAB6D5AD98176BD5D4B83F134CBE1309B5EE5DEBE0A5429EE89C56FE625305EBE93A6A43DD5AD664A5910633D53D260E8D79CDA75777DC969BCA3D763D6EEB06BCB7AD31DBE1EAF9FC13EA95948273A91A4E05E9CEC8413DC989FA6DA1FCB48C7AB8CC4347235C296FE1853EC8F18C168F6A737F55CCC16F09CE012117781639AE56D9870FA7B2D3CB9D99DE72F561C3B9EE234AA7B03531FB30DA460917B14D97728921322D6782252824AB1E60BE2E08789F957DAEA380D5BB0BFD2E23DE322FE48DC3F13A8B88D126CFC2D27780E9332DF7CB6DAD1070EDDB57AF1C7E7ACE99E711DC18C3936F6055DAE32C2F5670FBDA4C99AAE21CDCA8F219EEF84AABD3750A20A1362F5E70573970EF2B42097F2071F3DFCD85734E5F381B510154F0486C21B4485BA2700AB6069D3FF1DF8A469FA7FBFCEAA9F03AC229AF629804BFA83890F01BA2F4379CB2D6E358A03D12696A454CFAD89D46B65556E7B6F92F2ADD79AE8724E750FB835F2A657B08C6796723CD8EEA8C8281E0C7B9BA6FDE469C4BB92395CE6746C3761789339C20DB741DF546AF00E24B3299273B69F00BC695BD30571773C8BB25F9AEF8E191B4FD9DA7E32EFA68D4D17E6DD7163EB95B2BB63B6B6ADFD73CB96D6790BDD7A02AE9C4BA4B98C51C582DB126CB3C0399CF0E7011841E65166EF22D5195D4DD9A82D0C4B123D537D2A99C8589A38125F89A2996DBFBEF20DBFB1B39CA699AD2601B389375FFF1B79739A66DE9AB4C66DA4062B130B55E9DA2DEB5853EED3734A05AEF5A425F3BCCD676DBC597F4E99BF8328A5367B3477C4CF27D17710950C39757A24F6CAD7BDB077567E4111F6EFD85D9610ECF71409B66BBB664173411641BE790B12E5244284E61253E4C0967A125177816C0AD52CC69C3EEC4EE376ECA6638E9D0B729DD030A1D065ECCFBD5AC08B39014DFCD3ECE5BACCE3EB30FD8D9221BA0062BA2C367F4DDE25AEE714729F2B62421A08E65DF0882E1B4BCA22BBCBC702E92A201D81B8FA0AA7E816FBA10760F13599A17BBC8A6C607EEFF112D98F65045007D23E1075B58F4F5DB48C901F738CB23D7C820D3BFEC39BFF010452E06348540000, N'6.1.3-40302')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'9191ca24-873d-4a31-a096-6c87a795f672', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'010f3ab8-0622-4e0d-bd25-1efe03b11d49', N'Agent')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'23091c12-adfc-4bf6-a389-a66a57e93b72', N'Customer')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'abcfbe26-1c73-42a6-bfec-bc177c869f1f', N'Restaurant Admin')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4dfaf108-ca85-4f96-bd6f-8d245408cbc6', N'9191ca24-873d-4a31-a096-6c87a795f672')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'23d7d7c1-a0af-4615-95a7-9cb436a8e3fe', N'gagan@zenq.com', 0, N'AH+nHgbDU+QL8ujiVn64/3RxoMeFtjjhhvUchmzdwjksIA2v50MuLHspj9gI7A/f0w==', N'62f3d322-9cd7-4a03-b2c7-f38260ef20e7', NULL, 0, 0, NULL, 0, 0, N'gagan@zenq.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2a44bb97-fe6e-4537-ae97-7eaa4eb4e82b', N'kishorekompilla@gmail.com', 0, N'AN8x8na5zEzTamBYENniJvKEcVHjbEC231YLbFTy64CnA9s91jMxIgDNYaUuNLzAsw==', N'46a0580f-bfd9-4c64-88b4-aebfedc3c18e', NULL, 0, 0, NULL, 0, 0, N'kishorekompilla@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2ac3a5a8-e715-4a71-b892-6ef6c0101f81', N'gaganp@zenq.com', 1, N'AD2HdD1iPDTaX77C+UvbWTRXzc5uArfmOUmxpBA4iVGyMOdiKboW9MUcZekmeuApZA==', N'98942974-efe0-473a-ad5a-5f32f70825c5', NULL, 0, 0, NULL, 0, 0, N'gaganp@zenq.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'441a252f-ed58-4dc1-88d8-c412f6c1100d', N'gagan@aarushi.com', 1, N'AKICS1Nqsu7PyNB9pmZuMOs593bokDUdKP+KYtnfZAWDAeJdaNZtzFlAqVtUuI8V9Q==', N'8c5166ae-9814-4263-82e9-b14c09ea1ac4', NULL, 0, 0, NULL, 1, 0, N'gagan@aarushi.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4dfaf108-ca85-4f96-bd6f-8d245408cbc6', N'admin@example.com', 0, N'AJMCtIyRFLt71Aj9B4qSX1aFq9SRMoSr1RYS0spOTcEtaBPVPBNLCDrA73WEbgPyYA==', N'753e6b8e-dabf-4f12-81e8-f9313bd43a84', NULL, 0, 0, NULL, 0, 0, N'admin@example.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5356d8a0-00a0-4d1d-b18e-df5e01f261bc', N'gaganpentakota@gmail.com', 0, N'ADiUzZlmZcy6d+6Y+nKYQVmvI/oZnpE0VAuEY7vjFgobJh8ZYLqMPmA0RnvLut2Djw==', N'109686fd-3cd8-4fc0-ad9e-76409213d10a', NULL, 0, 0, NULL, 0, 0, N'gaganpentakota@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'83d24310-2c16-4b23-9a92-e1132595f75d', N'xyz@zenq.com', 1, N'AOm7GEfQJADYMWchJPNvx6Tgpj1x15wKW5cJz3D70C1VfQCP1g4IHoZ+mClaPuwoiw==', N'f2f6c23c-2fa8-4b00-8bf1-c5012beb4ac7', NULL, 0, 0, NULL, 0, 0, N'xyz@zenq.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'984ab14e-058c-413d-9958-02f4a9989fb9', N'ss@gmail.com', 1, N'APbmp1XdHlu514b0rw78Q+RTbEraH3nbOlCwWDCpJM4oCBOCRwfXG1YDUtei4s/dGw==', N'0e4c910c-45ef-4b9d-bd07-6b0e4bb3affd', NULL, 0, 0, NULL, 1, 0, N'ss@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a0ef5206-e4e4-4574-a028-4729215d6cc7', N's@g.com', 1, N'AMKCpCSWuUk2mra8+sLw+DCMQw4FESMw0n9DUe7J2VGt8nEVYlQHqWBKvS3kt/PtmQ==', N'6e3efe7b-2ec4-464e-9342-4f13b9ecca1b', NULL, 0, 0, NULL, 1, 0, N's@g.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b689dbfb-4ba8-4d2c-96e8-68b4676ca44d', N'g@gmail.com', 1, N'ANlIjdqs4pqaEwHn/qJdmwRgbCGlLanRQd/xY5efoCtbdUeTCUHjNPspQ/4sDYWEEA==', N'8e44d760-a59b-46c4-9416-ac25f7700af3', NULL, 0, 0, NULL, 0, 0, N'g@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'f7c8c82c-6f44-4873-a1e6-f91cfce9dbe9', N'sr@gmail.com', 1, N'ALrUZmrA8kSzbVNrLDkLoSIRK3YLQdVK9GdHpVXiFSQmJhvOfTtLfArYmuXTI3lbhg==', N'f69e9fdc-79ef-455a-b94f-8046e1f0963c', NULL, 0, 0, NULL, 1, 0, N'sr@gmail.com')
SET IDENTITY_INSERT [dbo].[tbl_Category] ON 

INSERT [dbo].[tbl_Category] ([CategoryID], [SectionID], [CategoryName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 1, N'Non-Veg', 1, 1, CAST(0x0000A4EE011F3E89 AS DateTime), 1, CAST(0x0000A4EE011F3E89 AS DateTime))
INSERT [dbo].[tbl_Category] ([CategoryID], [SectionID], [CategoryName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 1, N'Vegetarian', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
INSERT [dbo].[tbl_Category] ([CategoryID], [SectionID], [CategoryName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 2, N'Vegetarian', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
INSERT [dbo].[tbl_Category] ([CategoryID], [SectionID], [CategoryName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, 2, N'Non-Vegetarian', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
INSERT [dbo].[tbl_Category] ([CategoryID], [SectionID], [CategoryName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (5, 3, N'Sweets', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
INSERT [dbo].[tbl_Category] ([CategoryID], [SectionID], [CategoryName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (6, 3, N'Ice Cream', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Category] OFF
SET IDENTITY_INSERT [dbo].[tbl_Cuisine] ON 

INSERT [dbo].[tbl_Cuisine] ([CuisineID], [RestaurantID], [CuisineType], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 1, N'Chinese Items', 1, 1, CAST(0x0000A4ED001C50D4 AS DateTime), 1, CAST(0x0000A50100E4756E AS DateTime))
INSERT [dbo].[tbl_Cuisine] ([CuisineID], [RestaurantID], [CuisineType], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 1, N'Indian', 1, 1, CAST(0x0000A4EE00A5218A AS DateTime), 1, CAST(0x0000A4EE00A5218A AS DateTime))
INSERT [dbo].[tbl_Cuisine] ([CuisineID], [RestaurantID], [CuisineType], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 1, N'Japanese', 1, 1, CAST(0x0000A4F20113AD99 AS DateTime), 1, CAST(0x0000A4F20113AD99 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Cuisine] OFF
SET IDENTITY_INSERT [dbo].[tbl_Item] ON 

INSERT [dbo].[tbl_Item] ([ItemID], [CategoryID], [ItemName], [ItemDescription], [ItemImage], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 1, N'Chicken Hot & Sour Soup', N'Chicken Hot & Sour Soup is a hot and little spicy', N'Images/Item_1.jpg', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
INSERT [dbo].[tbl_Item] ([ItemID], [CategoryID], [ItemName], [ItemDescription], [ItemImage], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 2, N'Veg Hot & Sour Soup', N'Veg Hot & Sour Soup is a hot and little spicy', N'Images/Item_1.jpg', 1, 1, CAST(0x0000A4FD00CF20B3 AS DateTime), 1, CAST(0x0000A4FD00CF20B3 AS DateTime))
INSERT [dbo].[tbl_Item] ([ItemID], [CategoryID], [ItemName], [ItemDescription], [ItemImage], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 1, N'dosa', N'nice', N'Images', 1, 1, CAST(0x0000A54100E26C38 AS DateTime), 1, CAST(0x0000A54100E26C38 AS DateTime))
INSERT [dbo].[tbl_Item] ([ItemID], [CategoryID], [ItemName], [ItemDescription], [ItemImage], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, 1, N'puri', N'nice', N'Images', 1, 1, CAST(0x0000A54100E2751F AS DateTime), 1, CAST(0x0000A54100E2751F AS DateTime))
INSERT [dbo].[tbl_Item] ([ItemID], [CategoryID], [ItemName], [ItemDescription], [ItemImage], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (5, 4, N'ChukenBiryani', N'nice', N'Images', 1, 1, CAST(0x0000A54100E2FF20 AS DateTime), 1, CAST(0x0000A54100E2FF20 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Item] OFF
SET IDENTITY_INSERT [dbo].[tbl_ItemDetails] ON 

INSERT [dbo].[tbl_ItemDetails] ([ItemDetailsID], [ItemID], [ItemCode], [ItemSize], [SalesPrice], [TaxPrice], [Discount], [UOM], [CostPrice], [MBQ], [StackUOM], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 2, N'001', N'Medium', N'10', N'0', N'0', N'125 gms', N'0', N'0', N'500 Gms', 1, 1, CAST(0x0000A4FD00CF20B3 AS DateTime), 1, CAST(0x0000A4FD00CF20B3 AS DateTime))
INSERT [dbo].[tbl_ItemDetails] ([ItemDetailsID], [ItemID], [ItemCode], [ItemSize], [SalesPrice], [TaxPrice], [Discount], [UOM], [CostPrice], [MBQ], [StackUOM], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 2, N'002', N'Large', N'20', N'0', N'0', N'250 gms', N'0', N'0', N'500 Gms', 1, 1, CAST(0x0000A4FD00CF20B3 AS DateTime), 1, CAST(0x0000A4FD00CF20B3 AS DateTime))
INSERT [dbo].[tbl_ItemDetails] ([ItemDetailsID], [ItemID], [ItemCode], [ItemSize], [SalesPrice], [TaxPrice], [Discount], [UOM], [CostPrice], [MBQ], [StackUOM], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (7, 1, N'001', N'Medium', N'10', N'0', N'0', N'125 gms', N'0', N'0', N'500 Gms', 1, 1, CAST(0x0000A4FD00CFA440 AS DateTime), 1, CAST(0x0000A4FD00CFA440 AS DateTime))
INSERT [dbo].[tbl_ItemDetails] ([ItemDetailsID], [ItemID], [ItemCode], [ItemSize], [SalesPrice], [TaxPrice], [Discount], [UOM], [CostPrice], [MBQ], [StackUOM], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (8, 1, N'002', N'Large', N'20', N'0', N'0', N'250 gms', N'0', N'0', N'500 Gms', 1, 1, CAST(0x0000A4FD00CFA440 AS DateTime), 1, CAST(0x0000A4FD00CFA440 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_ItemDetails] OFF
SET IDENTITY_INSERT [dbo].[tbl_Lookup] ON 

INSERT [dbo].[tbl_Lookup] ([LookupID], [LookupCategoryID], [LookupName], [LookupCode], [LookupDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 1, N'Admin', N'AD', N'Super Admin of the site', 1, 1, CAST(0x0000A4E900D11D11 AS DateTime), 1, CAST(0x0000A54500A816C4 AS DateTime))
INSERT [dbo].[tbl_Lookup] ([LookupID], [LookupCategoryID], [LookupName], [LookupCode], [LookupDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 1, N'Agent', N'AG', N'Agent of the site where will have an access to multiple restaurants', 1, 1, CAST(0x0000A4E900D1550F AS DateTime), 1, CAST(0x0000A4E900D1550F AS DateTime))
INSERT [dbo].[tbl_Lookup] ([LookupID], [LookupCategoryID], [LookupName], [LookupCode], [LookupDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 1, N'RAdmin', N'RA', N'Restaurant Admin', 1, 1, CAST(0x0000A4E900D18B34 AS DateTime), 1, CAST(0x0000A4E900D18B34 AS DateTime))
INSERT [dbo].[tbl_Lookup] ([LookupID], [LookupCategoryID], [LookupName], [LookupCode], [LookupDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, 1, N'Customer', N'CA', N'Customer Admin', 1, 1, CAST(0x0000A4E900D1BD88 AS DateTime), 1, CAST(0x0000A4E900D1BD88 AS DateTime))
INSERT [dbo].[tbl_Lookup] ([LookupID], [LookupCategoryID], [LookupName], [LookupCode], [LookupDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (5, 1, N'pp', N'p ', N'pp', 0, 1, CAST(0x0000A54400E7BA8E AS DateTime), 1, CAST(0x0000A546005B4472 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Lookup] OFF
SET IDENTITY_INSERT [dbo].[tbl_LookupCategory] ON 

INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, N'CustomerType', N'CT', N'Customer Type used for Login Type', 1, 1, CAST(0x0000A50A00BD6FCC AS DateTime), 1, CAST(0x0000A50A00BD6FCC AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, N'Gender', N'G ', N'Gender', 0, 1, CAST(0x0000A50A00BD9DEB AS DateTime), 1, CAST(0x0000A50A00BDA75A AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, N'Cuisine', N'CU', N'Cuisine', 1, 1, CAST(0x0000A5110067CD6F AS DateTime), 1, CAST(0x0000A54500AF48C7 AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, N'ss', N'ss', N's', 0, 1, CAST(0x0000A5440088F066 AS DateTime), 1, CAST(0x0000A54400D59EBA AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (5, N'e', N'ee', N'e', 0, 1, CAST(0x0000A54400D60156 AS DateTime), 1, CAST(0x0000A54400D62824 AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (6, N's', N'ss', N's', 0, 1, CAST(0x0000A54500AEE3E6 AS DateTime), 1, CAST(0x0000A54500AF2629 AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (7, N'f', N'f ', N'f', 0, 1, CAST(0x0000A54500C05DE9 AS DateTime), 1, CAST(0x0000A54500C07952 AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (8, N'h', N'h ', N'h', 0, 1, CAST(0x0000A54500C5554B AS DateTime), 1, CAST(0x0000A54500C5F8E4 AS DateTime))
INSERT [dbo].[tbl_LookupCategory] ([LookupCategoryID], [LookupCategoryName], [LookupCategoryCode], [LookupCategoryDescription], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (9, N'section', N'ss', N'ss', 0, 1, CAST(0x0000A54500D1FEF0 AS DateTime), 1, CAST(0x0000A54500D2B275 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_LookupCategory] OFF
SET IDENTITY_INSERT [dbo].[tbl_Restaurant] ON 

INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, N'KFC Restaurant', N'KFC', N'KFC', N'12456874', N'Gagan', N'gaganpentakota@gmail.com', N'9989212324', 1, 1, CAST(0x0000A4F000D9AA60 AS DateTime), 1, CAST(0x0000A4F200DFE4EB AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, N'Dominos', N'Dominos', N'Dominos', N'123456789', N'kishore', N'kishorekompilla@gmail.com', N'9703815385', 1, 1, CAST(0x0000A4EC00EA3383 AS DateTime), 1, CAST(0x0000A4EC00EA3383 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, N'Pizza Hut', N'PH', N'PH', N'123456', N'Ajay', N'ajay.zenq@gmail.com', N'123456789', 1, 1, CAST(0x0000A4EC00EC5BB5 AS DateTime), 1, CAST(0x0000A4EC00EC5BB5 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, N'abc', N'abc', N'abc', N'123424342', N'dsdsfdsa', N'adsfdas@sdfasd.com', N'98327837324', 0, 1, CAST(0x0000A4EE005BA9D2 AS DateTime), 1, CAST(0x0000A53F0092682E AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (5, N'abc', N'abc', N'abc', N'123424342', N'dsdsf', N'adsfdas@sdfasd.com', N'98327837324', 0, 1, CAST(0x0000A4EE005BCC8B AS DateTime), 1, CAST(0x0000A4FD012EB9A5 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (6, N'KFC', N'KFC', N'KFC', N'12456874', N'Gagan', N'gaganpentakota@gmail.com', N'9989212324', 0, 1, CAST(0x0000A4F200E04840 AS DateTime), 1, CAST(0x0000A53F00926638 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (8, N'Thanet Kebab & Pizza', N'Thanet', N'Excellent Kebabs', N'1', N'Sarah', N'sara@email.com', N'01843520568', 0, 1, CAST(0x0000A4FA00EBFB4B AS DateTime), 1, CAST(0x0000A53F00926085 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (9, N'PulaoPacket', N'PP', N'No', N'1', N'Shankar', N'S@gmail.com', N'9898989898', 1, 1, CAST(0x0000A53F009355C7 AS DateTime), 1, CAST(0x0000A53F009355C7 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (10, N'Navayuga', N'NY', N'Good', N'1', N'SS', N'yuga@gmail.com', N'9898989898', 1, 1, CAST(0x0000A54100C07AF0 AS DateTime), 1, CAST(0x0000A54100C07AF0 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (11, N'bhawarchi', N'bhawarchi', N'bhawarchi', N'12', N's', N's@gmail.com', N'9800067891', 0, 1, CAST(0x0000A54100CBCCED AS DateTime), 1, CAST(0x0000A54500C70AB3 AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (12, N'KS Bakers', N'K S', N'Nice', N'12', N'p', N'l@gmail.com', N'9999999999', 0, 1, CAST(0x0000A54100DA9050 AS DateTime), 1, CAST(0x0000A54500C6F2DC AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (13, N'Ks Bakery', N'k', N'f', N'f', N'f', N'f', N'f', 0, 1, CAST(0x0000A54500C73657 AS DateTime), 1, CAST(0x0000A54500C7467A AS DateTime))
INSERT [dbo].[tbl_Restaurant] ([RestaurantID], [RestaurantName], [RestaurantShortName], [RestaurantDescription], [TAXID], [PrimaryContactName], [PrimaryContactEmail], [PrimaryContactNumber], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (14, N'f', N'f', N'f', N'f', N'f', N'f', N'f', 0, 1, CAST(0x0000A54500C78F97 AS DateTime), 1, CAST(0x0000A54500C79CF6 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Restaurant] OFF
SET IDENTITY_INSERT [dbo].[tbl_RestaurantDetails] ON 

INSERT [dbo].[tbl_RestaurantDetails] ([RestaurantDetailsID], [RestaurantID], [Address1], [Address2], [City], [State], [Country], [ZIPCode], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 1, N'Hitec City', N'Madhapur', N'Hyderabad', N'Telangana', N'India', N'500081', 1, 1, CAST(0x0000A4FC00C0E2D4 AS DateTime), 1, CAST(0x0000A4FC00C0E2D4 AS DateTime))
INSERT [dbo].[tbl_RestaurantDetails] ([RestaurantDetailsID], [RestaurantID], [Address1], [Address2], [City], [State], [Country], [ZIPCode], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 2, N'matrusri nagar', N'Miyapur', N'Hyderabad', N'Telangana', N'India', N'500081', 1, 1, CAST(0x0000A4FC00C1185F AS DateTime), 1, CAST(0x0000A4FC00C1185F AS DateTime))
INSERT [dbo].[tbl_RestaurantDetails] ([RestaurantDetailsID], [RestaurantID], [Address1], [Address2], [City], [State], [Country], [ZIPCode], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, 3, N'matrusri nagar', N'Paradise', N'Secundrabad', N'Telangana', N'India', N'500081', 1, 1, CAST(0x0000A4FC00C518A9 AS DateTime), 1, CAST(0x0000A4FC00C518A9 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_RestaurantDetails] OFF
SET IDENTITY_INSERT [dbo].[tbl_RestaurantImage] ON 

INSERT [dbo].[tbl_RestaurantImage] ([RestaurantImageID], [RestaurantID], [RestaurantImage]) VALUES (1, 1, N'Images/Restaurant-1.jpg')
INSERT [dbo].[tbl_RestaurantImage] ([RestaurantImageID], [RestaurantID], [RestaurantImage]) VALUES (2, 2, N'Images/Restaurant-1.jpg')
INSERT [dbo].[tbl_RestaurantImage] ([RestaurantImageID], [RestaurantID], [RestaurantImage]) VALUES (3, 3, N'Images/Restaurant-1.jpg')
INSERT [dbo].[tbl_RestaurantImage] ([RestaurantImageID], [RestaurantID], [RestaurantImage]) VALUES (4, 4, N'Images/Restaurant-1.jpg')
SET IDENTITY_INSERT [dbo].[tbl_RestaurantImage] OFF
SET IDENTITY_INSERT [dbo].[tbl_RestaurantTimings] ON 

INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 1, N'Monday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF2F AS DateTime), 1, CAST(0x0000A4FC00D9DF2F AS DateTime))
INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 1, N'Tuesday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF34 AS DateTime), 1, CAST(0x0000A4FC00D9DF34 AS DateTime))
INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 1, N'Wednesday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF34 AS DateTime), 1, CAST(0x0000A4FC00D9DF34 AS DateTime))
INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (4, 1, N'Thursday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF34 AS DateTime), 1, CAST(0x0000A4FC00D9DF34 AS DateTime))
INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (5, 1, N'Friday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF34 AS DateTime), 1, CAST(0x0000A4FC00D9DF34 AS DateTime))
INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (6, 1, N'Saturday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF34 AS DateTime), 1, CAST(0x0000A4FC00D9DF34 AS DateTime))
INSERT [dbo].[tbl_RestaurantTimings] ([RestaurantTimingsID], [RestaurantID], [Day], [StartTime], [EndTime], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (7, 1, N'Sunday', N'11:00', N'23:00', 1, 1, CAST(0x0000A4FC00D9DF34 AS DateTime), 1, CAST(0x0000A4FC00D9DF34 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_RestaurantTimings] OFF
SET IDENTITY_INSERT [dbo].[tbl_Section] ON 

INSERT [dbo].[tbl_Section] ([SectionID], [RestaurantID], [SectionName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (1, 1, N'Starters', 1, 1, CAST(0x0000A4EE011B74AD AS DateTime), 1, CAST(0x0000A4EE011B74AD AS DateTime))
INSERT [dbo].[tbl_Section] ([SectionID], [RestaurantID], [SectionName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (2, 1, N'Main Course', 1, 1, CAST(0x0000A4EE00C43CE2 AS DateTime), 1, CAST(0x0000A4EE00C43CE2 AS DateTime))
INSERT [dbo].[tbl_Section] ([SectionID], [RestaurantID], [SectionName], [IsActive], [CreatedBy], [CreatedOn], [LastEditBy], [LastEditOn]) VALUES (3, 1, N'Deserts', 1, 1, CAST(0x0000A4FD00CF20B2 AS DateTime), 1, CAST(0x0000A4FD00CF20B2 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Section] OFF
ALTER TABLE [dbo].[tbl_Agent] ADD  CONSTRAINT [DF_tbl_Agent_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tbl_Agent] ADD  CONSTRAINT [DF_tbl_Agent_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO
ALTER TABLE [dbo].[tbl_Cuisine] ADD  CONSTRAINT [DF_tbl_Cuisine_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tbl_Cuisine] ADD  CONSTRAINT [DF_tbl_Cuisine_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO
ALTER TABLE [dbo].[tbl_Lookup] ADD  CONSTRAINT [DF_tbl_Lookup_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tbl_Lookup] ADD  CONSTRAINT [DF_tbl_Lookup_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO
ALTER TABLE [dbo].[tbl_LookupCategory] ADD  CONSTRAINT [DF_tbl_LookupCategory_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tbl_LookupCategory] ADD  CONSTRAINT [DF_tbl_LookupCategory_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[tbl_Category]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Section_tbl_Category] FOREIGN KEY([SectionID])
REFERENCES [dbo].[tbl_Section] ([SectionID])
GO
ALTER TABLE [dbo].[tbl_Category] CHECK CONSTRAINT [FK_tbl_Section_tbl_Category]
GO
ALTER TABLE [dbo].[tbl_Cuisine]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_Cuisine] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO
ALTER TABLE [dbo].[tbl_Cuisine] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_Cuisine]
GO
ALTER TABLE [dbo].[tbl_Item]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Item_tbl_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[tbl_Category] ([CategoryID])
GO
ALTER TABLE [dbo].[tbl_Item] CHECK CONSTRAINT [FK_tbl_Item_tbl_Category]
GO
ALTER TABLE [dbo].[tbl_Lookup]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Lookup_tbl_LookupCategory] FOREIGN KEY([LookupCategoryID])
REFERENCES [dbo].[tbl_LookupCategory] ([LookupCategoryID])
GO
ALTER TABLE [dbo].[tbl_Lookup] CHECK CONSTRAINT [FK_tbl_Lookup_tbl_LookupCategory]
GO
ALTER TABLE [dbo].[tbl_Order]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Order_tbl_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[tbl_Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[tbl_Order] CHECK CONSTRAINT [FK_tbl_Order_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_Order]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Order_tbl_Restaurant] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO
ALTER TABLE [dbo].[tbl_Order] CHECK CONSTRAINT [FK_tbl_Order_tbl_Restaurant]
GO
ALTER TABLE [dbo].[tbl_OrderDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_OrderDetails_tbl_ItemDetails] FOREIGN KEY([ItemDetailsID])
REFERENCES [dbo].[tbl_ItemDetails] ([ItemDetailsID])
GO
ALTER TABLE [dbo].[tbl_OrderDetails] CHECK CONSTRAINT [FK_tbl_OrderDetails_tbl_ItemDetails]
GO
ALTER TABLE [dbo].[tbl_OrderDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_OrderDetails_tbl_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[tbl_Order] ([OrderID])
GO
ALTER TABLE [dbo].[tbl_OrderDetails] CHECK CONSTRAINT [FK_tbl_OrderDetails_tbl_Order]
GO
ALTER TABLE [dbo].[tbl_RestaurantDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantDetails] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO
ALTER TABLE [dbo].[tbl_RestaurantDetails] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantDetails]
GO
ALTER TABLE [dbo].[tbl_RestaurantTimings]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantTimings] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO
ALTER TABLE [dbo].[tbl_RestaurantTimings] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantTimings]
GO
ALTER TABLE [dbo].[tbl_Section]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_Section] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO
ALTER TABLE [dbo].[tbl_Section] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_Section]
GO
