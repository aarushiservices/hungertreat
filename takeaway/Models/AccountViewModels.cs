﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace takeaway.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RestaurantViewModel
    {
        [Required]
        [Display(Name = "Restaurant ID")]
        public int RestaurantID { get; set; }

        [Required]
        [Display(Name = "Restaurant Name")]
        public string RestaurantName { get; set; }

        [Required]
        [Display(Name = "Restaurant ShortName")]
        public string RestaurantShortName { get; set; }

        [Required]
        [Display(Name = "Restaurant Description")]
        public string RestaurantDescription { get; set; }

        [Required]
        [Display(Name = "TAX ID")]
        public string TAXID { get; set; }

        [Required]
        [Display(Name = "Primary ContactName")]
        public string PrimaryContactName { get; set; }

        [Required]
        [Display(Name = "Primary ContactEmail")]
        public string PrimaryContactEmail { get; set; }

        [Required]
        [Display(Name = "Primary Contact Number")]
        public string PrimaryContactNumber { get; set; }

        [Required]
        [Display(Name = "IsActive")]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Required]
        [Display(Name = "Created On")]
        public System.DateTime CreatedOn { get; set; }

        [Required]
        [Display(Name = "Last EditBy")]
        public int LastEditBy { get; set; }

        [Required]
        [Display(Name = "Last EditOn")]
        public System.DateTime LastEditOn { get; set; }
    }

    public class CuisineViewModel
    {
        [Required]
        [Display(Name = "Cuisine ID")]
        public int CuisineID { get; set; }

        [Required]
        [Display(Name = "Restaurant ID")]
        public int RestaurantID { get; set; }

        [Required]
        [Display(Name = "Cuisine Type")]
        public string CuisineType { get; set; }

        [Required]
        [Display(Name = "IsActive")]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Required]
        [Display(Name = "Created On")]
        public System.DateTime CreatedOn { get; set; }

        [Required]
        [Display(Name = "Last EditBy")]
        public int LastEditBy { get; set; }

        [Required]
        [Display(Name = "Last EditOn")]
        public System.DateTime LastEditOn { get; set; }
    }

    public class SectionViewModel
    {
        [Required]
        [Display(Name = "Section ID")]
        public int SectionID { get; set; }

        [Required]
        [Display(Name = "Restaurant ID")]
        public int RestaurantID { get; set; }

        [Required]
        [Display(Name = "Section Name")]
        public string SectionName { get; set; }

        [Required]
        [Display(Name = "IsActive")]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Required]
        [Display(Name = "Created On")]
        public System.DateTime CreatedOn { get; set; }

        [Required]
        [Display(Name = "Last EditBy")]
        public int LastEditBy { get; set; }

        [Required]
        [Display(Name = "Last EditOn")]
        public System.DateTime LastEditOn { get; set; }

        public ICollection<CategoryViewModel> categories { get; set; }
    }

    public class CategoryViewModel
    {
        [Required]
        [Display(Name = "Category ID")]
        public int CategoryID { get; set; }

        [Required]
        [Display(Name = "Section ID")]
        public int SectionID { get; set; }

        [Required]
        [Display(Name = "Section Name")]
        public string SectionName { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [Required]
        [Display(Name = "IsActive")]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Required]
        [Display(Name = "Created On")]
        public System.DateTime CreatedOn { get; set; }

        [Required]
        [Display(Name = "Last EditBy")]
        public int LastEditBy { get; set; }

        [Required]
        [Display(Name = "Last EditOn")]
        public System.DateTime LastEditOn { get; set; }

        public ICollection<ItemViewModel> items { get; set; }
    }

    public class RestaurantDetailViewModel
    {
        public int RestaurantDetailsID { get; set; }
        public int RestaurantID { get; set; }
        public string RestaurantName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZIPCode { get; set; }
        public string RestaurantImage { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }
    }

    public class ItemViewModel
    {
        public int ItemID { get; set; }
        public int CategoryID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemImage { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }

        public ICollection<ItemDetailsViewModel> itemsDetails { get; set; }
        public List<ItemDetailsViewModel> SubitemDetails { get; set; }
    }

    public class ItemDetailsViewModel
    {
        public int ItemDetailsID { get; set; }
        public int ItemID { get; set; }
        public string ItemCode { get; set; }
        public string ItemSize { get; set; }
        public string SalesPrice { get; set; }
        public string TaxPrice { get; set; }
        public string Discount { get; set; }
        public string UOM { get; set; }
        public string CostPrice { get; set; }
        public string MBQ { get; set; }
        public string StackUOM { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }
    }

    public class RestaurantMenuViewModel
    {
        [Required]
        [Display(Name = "Section ID")]
        public int SectionID { get; set; }

        [Required]
        [Display(Name = "Restaurant ID")]
        public int RestaurantID { get; set; }

        [Required]
        [Display(Name = "Section Name")]
        public string SectionName { get; set; }

        [Required]
        [Display(Name = "Category ID")]
        public int CategoryID { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemImage { get; set; }
        public int ItemDetailsID { get; set; }
        public string ItemCode { get; set; }
        public string ItemSize { get; set; }
        public string SalesPrice { get; set; }
        public string TaxPrice { get; set; }
        public string Discount { get; set; }
        public string UOM { get; set; }
        public int Quantity { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }


    //Shankar
    public class LookUpCategoryViewModel
    {
        public int LookupCategoryID { get; set; }
        public string LookupCategoryName { get; set; }
        public string LookupCategoryCode { get; set; }
        public string LookupCategoryDescription { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }

    }
    public class LookUpViewModel
    {
        public int LookupID { get; set; }
        public int LookupCategoryID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        [Display(Name = "LookupName")]
        public string LookupName { get; set; }
        public string LookupCode { get; set; }
        public string LookupDescription { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }

    }

    public class CustomerViewModel
    {
    
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZIPCode { get; set; }      
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }

    }

}
