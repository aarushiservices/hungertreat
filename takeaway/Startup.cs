﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(takeaway.Startup))]
namespace takeaway
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
