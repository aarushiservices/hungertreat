//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace takeaway.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_RestaurantImage
    {
        public int RestaurantImageID { get; set; }
        public int RestaurantID { get; set; }
        public string RestaurantImage { get; set; }
    }
}
