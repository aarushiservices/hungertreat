//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace takeaway.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Category
    {
        public tbl_Category()
        {
            this.tbl_Item = new HashSet<tbl_Item>();
        }
    
        public int CategoryID { get; set; }
        public int SectionID { get; set; }
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }
    
        public virtual ICollection<tbl_Item> tbl_Item { get; set; }
        public virtual tbl_Section tbl_Section { get; set; }
    }
}
