//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace takeaway.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Item
    {
        public tbl_Item()
        {
            this.tbl_ItemDetails = new HashSet<tbl_ItemDetails>();
        }
    
        public int ItemID { get; set; }
        public int CategoryID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemImage { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int LastEditBy { get; set; }
        public System.DateTime LastEditOn { get; set; }
    
        public virtual tbl_Category tbl_Category { get; set; }
        public virtual ICollection<tbl_ItemDetails> tbl_ItemDetails { get; set; }
    }
}
