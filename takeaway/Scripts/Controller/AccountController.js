﻿AccountApp.controller('AccountController', function ($scope, AccountService) {
    var restaurantID;
    var cuisineID;
    var totalPrice=0;

    //$scope.gridOptions.sortInfo = {
    //    fields: ['RestaurantName'],
    //    directions: ['asc'],
    //    columns: [0, 1]
    //};

    $scope.totalServerItems = 0;

    $scope.pagingOptions = {
        pageSizes: [5, 10, 20],
        pageSize: 5,
        currentPage: 1
    };
    //retrieve restaurant details
    $scope.GetRestaurants = function () {
        var promiseGet = AccountService.GetRestaurants();
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModalEdit" ng-click="editRestaurantDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="deleteRestaurants(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.RestaurantsData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridOptions = {
            data: 'RestaurantsData',
            jqueryUITheme: true,
            pagingOptions: $scope.pagingOptions,
            enablePaging: true,
            showFooter: true,
            columnDefs: [
                            {
                                displayName: "Restaurant Name",
                                field: "RestaurantName",
                                width: 200,
                            },
                            {
                                displayName: "Contact Name",
                                field: "PrimaryContactName",
                                width: 200,
                            },
                            {
                                displayName: "Email ID",
                                field: "PrimaryContactEmail",
                                width: 200,
                            },
                            {
                                displayName: "Number",
                                field: "PrimaryContactNumber",
                                width: 100,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //retrieve restaurant names
    $scope.GetRestaurantNames = function () {
        var promiseGet = AccountService.GetRestaurantNames();
        promiseGet.then(function (pl) {
            $scope.RestaurantsData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
        var CuisinesData = null;
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModalEdit" ng-click="editCuisineDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="updateRestaurants(row.entity)" />';
        $scope.gridCuisineOptions = {
            data: 'CuisinesData',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Restaurant ID",
                                field: "RestaurantID",
                                width: 200,
                            },
                            {
                                displayName: "Cuisine Type",
                                field: "CuisineType",
                                width: 200,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 200,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //on Edit click
    $scope.editRestaurantDetails = function (pl) {
       // alert(pl.RestaurantName);
        //alert($scope.RestaurantName);
        restaurantID = pl.RestaurantID;
        $scope.EditRestaurantName = pl.RestaurantName;
        $scope.EditRestaurantShortName = pl.RestaurantShortName;
        $scope.EditRestaurantDescription = pl.RestaurantDescription;
        $scope.EditTAXID = pl.TAXID;
        $scope.EditPrimaryContactName = pl.PrimaryContactName;
        $scope.EditPrimaryContactEmail = pl.PrimaryContactEmail;
        $scope.EditPrimaryContactNumber = pl.PrimaryContactNumber;
        $scope.EditIsActive = pl.IsActive;
    }
    //Update Restaurant details
    $scope.updateRestaurantDetails = function () {
        var updateRestaurantViewModel = {
            RestaurantID: restaurantID,
            RestaurantName: $scope.EditRestaurantName,
            RestaurantShortName: $scope.EditRestaurantShortName,
            RestaurantDescription: $scope.EditRestaurantDescription,
            TAXID: $scope.EditTAXID,
            PrimaryContactName: $scope.EditPrimaryContactName,
            PrimaryContactEmail: $scope.EditPrimaryContactEmail,
            PrimaryContactNumber: $scope.EditPrimaryContactNumber,
            IsActive: $scope.EditIsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.updateRestaurantDetails(updateRestaurantViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearEditRestaurantDetails();
            $scope.GetRestaurants();

        }, function (err) {
            alert("data not saved..");
            $scope.clearEditRestaurantDetails();
        });
    };

    //Delete Restaurant details
    $scope.deleteRestaurants = function (data) {
        var Restaurants = {
            RestaurantID: parseInt(data.RestaurantID, 10),
            IsActive: false,
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.deleteRestaurantDetails(Restaurants);
        promisePost.then(function (pl) {
            alert("data deleted..");
            $scope.GetRestaurants();

        }, function (err) {
            console.log("Err" + err);
        });
    };

    //save Restaurant details
    $scope.saveRestaurantDetails = function () {
        var RestaurantViewModel = {
            RestaurantName: $scope.RestaurantName,
            RestaurantShortName: $scope.RestaurantShortName,
            RestaurantDescription: $scope.RestaurantDescription,
            TAXID: $scope.TAXID,
            PrimaryContactName: $scope.PrimaryContactName,
            PrimaryContactEmail: $scope.PrimaryContactEmail,
            PrimaryContactNumber: $scope.PrimaryContactNumber,
            IsActive: $scope.IsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveRestaurantDetails(RestaurantViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearRestaurantDetails();
            $scope.GetRestaurants();

        }, function (err) {
            alert("data not saved..");
            $scope.clearRestaurantDetails();
        });
    }

    $scope.clearRestaurantDetails = function () {
        $scope.RestaurantName = null;
        $scope.RestaurantShortName = null;
        $scope.RestaurantDescription = null;
        $scope.TAXID = null;
        $scope.PrimaryContactName = null;
        $scope.PrimaryContactEmail = null;
        $scope.PrimaryContactNumber = null;
        $scope.IsActive = null;
    }

    $scope.clearEditRestaurantDetails = function () {
        $scope.EditRestaurantName = null;
        $scope.EditRestaurantShortName = null;
        $scope.EditRestaurantDescription = null;
        $scope.EditTAXID = null;
        $scope.EditPrimaryContactName = null;
        $scope.EditPrimaryContactEmail = null;
        $scope.EditPrimaryContactNumber = null;
        $scope.EditIsActive = null;
    }

    $scope.GetCuisines = function () {
  
        //var RestaurantID = 1;
        var RestaurantID = $scope.RestaurantID;
        var promiseGet = AccountService.GetCuisines(RestaurantID);
        var removeTemplate = '<input type="button" value="Edit" data-toggle="modal" data-target="#myModal" ng-click="editCuisineDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="updateRestaurants(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.CuisinesData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridCuisineOptions = {
            data: 'CuisinesData',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Restaurant ID",
                                field: "RestaurantID",
                                width: 200,
                            },
                            {
                                displayName: "Cuisine Type",
                                field: "CuisineType",
                                width: 200,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 200,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //on Edit click
    $scope.editCuisineDetails = function (pl) {
        // alert(pl.RestaurantName);
        //alert($scope.RestaurantName);
        cuisineID = pl.CuisineID;
        $scope.EditCuisineType = pl.CuisineType;
        $scope.EditIsActive = pl.IsActive;
    }
    //Update Restaurant details
    $scope.updateCuisineDetails = function () {
        var updateCuisineViewModel = {
            CuisineID: cuisineID,
            CuisineType: $scope.EditCuisineType,
            IsActive: $scope.EditIsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.updateCuisineDetails(updateCuisineViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearCuisineDetails();
            $scope.GetCuisines();

        }, function (err) {
            alert("data not saved..");
            $scope.clearCuisineDetails();
        });
    };
    //save Restaurant details
    $scope.saveCuisineDetails = function () {
        var CuisineViewModel = {
            RestaurantID: 1,//$scope.model.RestaurantID,
            CuisineType: $scope.model.CuisineType,
            IsActive: $scope.model.IsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveCuisineDetails(CuisineViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearCuisineDetails();
            $scope.GetCuisines();

        }, function (err) {
            alert("data not saved..");
            $scope.clearCuisineDetails();
        });
    }

    $scope.clearCuisineDetails = function () {
        $scope.EditCuisineType = null;
        $scope.EditIsActive = null;
    }

    //get Sections
    $scope.GetSections = function () {
        var promiseGet = AccountService.GetSections();
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModal" ng-click="editRestaurantDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="updateRestaurants(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.SectionsData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridOptions = {
            data: 'SectionsData',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Restaurant ID",
                                field: "RestaurantID",
                                width: 200,
                            },
                            {
                                displayName: "Section Name",
                                field: "SectionName",
                                width: 200,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 200,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //save Restaurant details
    $scope.saveSectionDetails = function () {
        var SectionViewModel = {
            RestaurantID: 1,//$scope.model.RestaurantID,
            SectionName: $scope.model.SectionName,
            IsActive: $scope.model.IsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveSectionDetails(SectionViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearSectionDetails();
            $scope.GetSections();

        }, function (err) {
            alert("data not saved..");
            $scope.clearSectionDetails();
        });
    }

    $scope.clearSectionDetails = function () {
        $scope.model.SectionName = null;
        $scope.model.IsActive = null;
    }

    $scope.GetCategories = function () {
        var SectionID = $scope.SectionID;
        var promiseGet = AccountService.GetCategories(SectionID);
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModal" ng-click="editRestaurantDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="updateRestaurants(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.CategoriesData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridOptions = {
            data: 'CategoriesData',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Section Name",
                                field: "SectionName",
                                width: 200,
                            },
                            {
                                displayName: "Category Name",
                                field: "CategoryName",
                                width: 200,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 200,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //save Restaurant details
    $scope.saveCategoryDetails = function () {
        var CategoryViewModel = {
            SectionID: 1,//$scope.model.RestaurantID,
            CategoryName: $scope.model.CategoryName,
            IsActive: $scope.model.IsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveCategoryDetails(CategoryViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearCategoryDetails();
            $scope.GetCategories();

        }, function (err) {
            alert("data not saved..");
            $scope.clearCategoryDetails();
        });
    }

    $scope.clearCategoryDetails = function () {
        $scope.model.CategoryName = null;
        $scope.model.IsActive = null;
    }

    $scope.GetCities = function () {
        var promiseGet = AccountService.GetCities();
        var basicCellTemplate = '<a href="/Home/Menu?RestaurantID={{row.entity.RestaurantID}}"><image src="../{{row.getProperty(col.field)}}"/></a>';
        //var basicCellTemplate = '<a href="/Home/Menu?RestaurantID=1"><image src="../{{row.getProperty(col.field)}}"/></a>';
        var basicCellTemplateOne = '<div>{{row.getProperty(col.field)}}</div>';
        promiseGet.then(function (pl) {
            $scope.CitiesData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
       
        var RestaurantDetailsData = null;
        $scope.gridRestaurantDetailsOptions = {
            data: 'RestaurantDetailsData',
            jqueryUITheme: true,
            rowHeight: 200,
            headerRowHeight: 0,
            columnDefs: [
                            {
                                displayName: "",
                                field: 'RestaurantImage',
                                width: 200,
                                heignt:200,
                                cellTemplate: basicCellTemplate
                            },
                            {
                                displayName: "",
                                field: 'RestaurantName',
                                width: 200,
                                cellTemplate: basicCellTemplateOne
                            }
                            ,
                            {
                                displayName: "",
                                field: 'Address1',
                                width: 200,
                                cellTemplate: basicCellTemplateOne
                            }
            ]

        };
        
    }

    $scope.GetAreas = function () {
        //var RestaurantID = 1;
        var _cityName = $scope.CityName;
        var promiseGet = AccountService.GetAreas(_cityName);
        promiseGet.then(function (pl) {
            $scope.AreasData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
    }

    $scope.searchRestaurantDetails = function () {
        var basicCellTemplate = '<a href="/Home/Menu?RestaurantID={{row.entity.RestaurantID}}"><image src="../{{row.getProperty(col.field)}}"/></a>';
        //var basicCellTemplate = '<a href="/Home/Menu?RestaurantID=1"><image src="../{{row.getProperty(col.field)}}"/></a>';
        var basicCellTemplateOne = '<div>{{row.getProperty(col.field)}}</div>';
        var RestaurantDetailsData = null;
        $scope.gridRestaurantDetailsOptions = {
            data: 'RestaurantDetailsData',
            jqueryUITheme: true,
            rowHeight: 200,
            headerRowHeight: 0,
            columnDefs: [
                            {
                                displayName: "",
                                field: 'RestaurantImage',
                                width: 200,
                                heignt: 200,
                                cellTemplate: basicCellTemplate
                            },
                            {
                                displayName: "",
                                field: 'RestaurantName',
                                width: 200,
                                cellTemplate: basicCellTemplateOne
                            }
                            ,
                            {
                                displayName: "",
                                field: 'Address1',
                                width: 200,
                                cellTemplate: basicCellTemplateOne
                            }
            ]

        };
        if ($scope.ZIPCode != null)
        {
            var RestaurantDetailViewModel =
           {
               ZIPCode: $scope.ZIPCode
           }
            //var basicCellTemplate = '<image src="~/{{row.getProperty(col.field)}}"/>';
            var promiseGet = AccountService.RetrieveRestaurantDetails(RestaurantDetailViewModel);
            promiseGet.then(function (pl) {
                $scope.RestaurantDetailsData = pl;
            },
                  function (errorPl) {
                      alert("Restaurant details not found");
                      console.log("Some Error in Getting Records." + errorPl);
                  });


            $scope.gridRestaurantDetailsOptions = {
                data: 'RestaurantDetailsData',
                jqueryUITheme: true,
                rowHeight: 200,
                headerRowHeight: 0,
                columnDefs: [
                                {
                                    field: 'RestaurantImage',
                                    width: 200,
                                    cellTemplate: basicCellTemplate
                                },
                                {
                                    field: 'RestaurantName',
                                    width: 200,
                                    cellTemplate: basicCellTemplateOne
                                }
                                ,
                                {
                                    field: 'Address1',
                                    width: 200,
                                    cellTemplate: basicCellTemplateOne
                                }
                ]

            };
        }
       
    }

    $scope.GetRestaurantMenu = function(RestaurantID)
    {
        alert(RestaurantID)
        var _RestaurantID = parseInt(RestaurantID, 10);
        var promiseGet = AccountService.GetRestaurantMenu(_RestaurantID);
        var basicCellTemplate = '<img ng-src="../Images/plusButton.png" ng-click="AddToCart(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.RestaurantMenuData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
        $scope.gridRestaurantMenuOptions = {
            data: 'RestaurantMenuData',
            jqueryUITheme: true,
            headerRowHeight: 0,
            columnDefs: [
                            {
                                displayName: "SectionName",
                                field: 'SectionName',
                                visible:false
                            },
                            {
                                displayName: "CategoryName",
                                field: 'CategoryName',
                                visible: false
                            },
                            {
                                displayName: "",
                                field: 'ItemName',
                                visible: false
                            },
                            {
                                displayName: "",
                                field: 'ItemCode',
                                visible: false
                            },
                            {
                                displayName: "",
                                field: 'ItemSize',
                                width: 130
                            },
                            {
                                displayName: "",
                                field: 'SalesPrice',
                                width: 130
                            },
                            {
                                displayName: "",
                                field: 'TaxPrice',
                                width: 100
                            },
                            {
                                displayName: "",
                                field: 'Discount',
                                width: 80
                            },
                            {
                                displayName: "",
                                field: 'UOM',
                                width: 200
                            },
                            {
                                displayName: "",
                                width: 20,
                                cellTemplate: basicCellTemplate
                            }
            ],
            groups:['SectionName','CategoryName','ItemName']
        };
        
        $scope.RestaurantMenuSelectedItems();
    }

    $scope.RestaurantMenuItems = [];

    $scope.RestaurantMenuSelectedItems = function()
    {
        var basicCellTemplateOne = '<img ng-src="../Images/deleteButton.png" ng-click="RemoveFromCart(row.entity,row.entity.Quantity)"/>';
        
        var basicCellTemplateQuanity = '<input type="text" id="Quantity" ng-model="Quantity" ng-change="countTotalPrice(row.entity,Quantity)" numbers-only="numbers-only"/>';
        //var basicCellTemplateTotal = '<label class="control-label"></label>';
        $scope.gridRestaurantMenuItemsOptions = {
            data: 'RestaurantMenuItems',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Item Name",
                                field: 'ItemName',
                                width: 200
                            },
                            {
                                displayName: "Item Size",
                                field: 'ItemSize',
                                width: 130
                            },
                            {
                                displayName: "Price",
                                field: 'SalesPrice',
                                width: 130
                            },
                            {
                                displayName: "Qty",
                                width: 50,
                                cellTemplate: basicCellTemplateQuanity
                            },
                            {
                                displayName: "",
                                width: 20,
                                cellTemplate: basicCellTemplateOne
                            }
            ]
        };
    }

    $scope.AddToCart = function(data)
    {
        alert('1')
        var isexits = false;
        var isNotExits = false;
        for (i in $scope.RestaurantMenuItems) {
            if ($scope.RestaurantMenuItems[i].ItemDetailsID != data.ItemDetailsID) {
                isNotExits = true;
            }
            else
            {
                isexits = true;
                break;
            }
        }
        if ($scope.RestaurantMenuItems.length == 0 || isexits == false)
        {
            $scope.newMenuItem = data;
            $scope.RestaurantMenuItems.push($scope.newMenuItem);
        }
        var basicCellTemplateOne = '<img ng-src="../Images/deleteButton.png" ng-click="RemoveFromCart(row.entity,row.entity.Quantity)"/>';
        var basicCellTemplateQuanity = '<input type="text" id="quantity" ng-model="quantity" ng-change="countTotalPrice(row.entity,quantity)" numbers-only="numbers-only"/>';
        //var basicCellTemplateTotal = '<label class="control-label"></label>';
        $scope.gridRestaurantMenuItemsOptions = {
            data: 'RestaurantMenuItems',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Item Name",
                                field: 'ItemName',
                                width: 200
                            },
                            {
                                displayName: "Item Size",
                                field: 'ItemSize',
                                width: 130
                            },
                            {
                                displayName: "Price",
                                field: 'SalesPrice',
                                width: 130
                            },
                            {
                                displayName: "Qty",
                                width: 50,
                                cellTemplate: basicCellTemplateQuanity
                            },
                            {
                                displayName: "",
                                width: 20,
                                cellTemplate: basicCellTemplateOne
                            }
            ]
        };
    }

    function GetSections($scope) {
        alert($scope);
    }

    $scope.RemoveFromCart = function(data)
    {
        //search MenuItem with given id and delete it
        for (i in $scope.RestaurantMenuItems) {
            if ($scope.RestaurantMenuItems[i].ItemDetailsID == data.ItemDetailsID) {
                $scope.RestaurantMenuItems.splice(i, 1);
                $scope.newMenuItem = {};
            }
        }
        var basicCellTemplateOne = '<img ng-src="../Images/deleteButton.png" ng-click="RemoveFromCart(row.entity,row.entity.Quantity)"/>';
        var basicCellTemplateQuanity = '<input type="text" id="quantity" ng-model="quantity" ng-change="countTotalPrice(row.entity,quantity)" numbers-only="numbers-only"/>';
        //var basicCellTemplateTotal = '<label class="control-label"></label>';
        $scope.gridRestaurantMenuItemsOptions = {
            data: 'RestaurantMenuItems',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Item Name",
                                field: 'ItemName',
                                width: 200
                            },
                            {
                                displayName: "Item Size",
                                field: 'ItemSize',
                                width: 130
                            },
                            {
                                displayName: "Price",
                                field: 'SalesPrice',
                                width: 130
                            },
                            {
                                displayName: "Qty",
                                width: 50,
                                cellTemplate: basicCellTemplateQuanity
                            },
                            {
                                displayName: "",
                                width: 20,
                                cellTemplate: basicCellTemplateOne
                            }
            ]
        };
        var salesPrice = data.SalesPrice;
        var result = salesPrice.replace('Sales Price:', '');
        var quantity = quantity;
        totalPrice -= (result * quantity);
        $scope.totalPrice = totalPrice;
    }

    $scope.countTotalPrice = function(data,quantity)
    {
        var salesPrice = data.SalesPrice;
        var result = salesPrice.replace('Sales Price:','');
        var quantity = quantity;
        totalPrice += (result * quantity);
        $scope.totalPrice = totalPrice;
        //alert(totalPrice);
    }

    function GetSiteRoot() {
        var rootPath = window.location.protocol + "//" + window.location.host + "/";
        if (window.location.hostname == "localhost") {
            var path = window.location.pathname;
            if (path.indexOf("/") == 0) {
                path = path.substring(1);
            }
            path = path.split("/", 1);
            if (path != "") {
                rootPath = rootPath + path + "/";
            }
        }
        return rootPath;
    }

    //save Items details
    $scope.saveItems = function () {
        var upload = document.getElementById("ItemImage").value;
        var Uplodedfile = document.getElementById("ItemImage").files[0];
        var dt = new Date();
        var rootPath = window.location.protocol + "//" + window.location.host + "/" + "Images/" + Uplodedfile.name;
        alert(rootPath);
        alert(copy(upload, rootPath));
       // var filepath = Uplodedfile.Move("~/Images")
       // 
       // var filepath = Path.Combine(Server.MapPath("~/Images/") + Uplodedfile.FileName + dt.getFullYear.toString() + dt.getMonth.toString() + dt.getDay.toString() + dt.getHours.toString() + dt.getMinutes.toString() + dt.getSeconds.toString());
        //alert(filepath);
        //alert(Uplodedfile.SaveAs(filepath));
        //var filepath = document.getElementById("ItemImage").value;
        
        // prepare a relative path to be stored in the database and used to display later on.
       // path = Url.Content(Path.Combine("~/Images/" + file.FileName));
        var ItemViewModel ={
            CategoryID: $scope.CategoryID,
            ItemName: $scope.ItemName,            
            ItemDescription: $scope.ItemDescription,
            ItemIngredients: $scope.ItemIngredients,
          //  ItemImage:$scope.ItemImage,
            IsActive: true,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveItems(ItemViewModel);
        promisePost.then(function (res) {
            $scope.ItemGridData = res.data;
            // alert(p1.data);
            alert("data saved..");
            //$scope.ItemID = p1.data;
                //var promiseGet = AccountService.GetItemsByID($scope.ItemID);
                ////var removeTemplate = '<input type="button" value="+" data-toggle="modal" data-target="#myModal" ng-click="GetItemsDetails(row.entity)" />';
                //promiseGet.then(function (pl) {
                //    $scope.CategoriesGridData = pl;
                //},
                //      function (errorPl) {

                //          console.log("Some Error in Getting Records." + errorPl);
                //      });

                $scope.gridItemsOption = {
                    data: 'CategoriesGridData',
                   jqueryUITheme: true,
                   columnDefs: [

                                    {
                                        displayName: "Item ID",
                                        field: "ItemID",
                                        width: 100,
                                    },

                                    {
                                        displayName: "Category ID",
                                        field: "CategoryID",
                                        width: 100,
                                    },
                                     {
                                         displayName: "Item Name",
                                         field: "ItemName",
                                         width: 100,
                                     },
                                    {
                                        displayName: "Is Active",
                                        field: "IsActive",
                                        width: 100,
                                    },
                                    {
                                        displayName: "Sub Items",
                                        field: "SubitemDetails",
                                        width: 200,
                                    },

                    ]

                };            

        }, function (err) {
            alert("data not saved..");
            $scope.clearsaveItems();
        });
    }

    $scope.saveSubItemsItems = function (data) {
        $scope.ItemID = data;
        var ItemViewModel = {
            ItemID: $scope.ItemID,
            ItemSize: $scope.ItemSize,
            ItemCode: $scope.ItemCode,
            UOM: $scope.UOM,
            IsActive: true,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveSubItemsItems(ItemViewModel);
        promisePost.then(function (Res) {
            $scope.ItemGridData = Res.data;
            alert("data saved..");           
        }, function (err) {
            alert("data not saved..");
            $scope.clearsaveItems();
        });
    }


    $scope.RetriveCategories = function () {
        var SectionID = $scope.SectionID;
        var promiseGet = AccountService.RetriveCategories(SectionID);
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModal" ng-click="editRestaurantDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="updateRestaurants(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.CategoriesData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridOptions = {
            data: 'CategoriesData',
            jqueryUITheme: true,
            columnDefs: [
                            {
                                displayName: "Section Name",
                                field: "SectionName",
                                width: 200,
                            },
                            {
                                displayName: "Category Name",
                                field: "CategoryName",
                                width: 200,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 200,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //display Items
    $scope.GetItems = function () {
        var CategoryID = $scope.CategoryID;
        var promiseGet = AccountService.GetItems(CategoryID);
        //var removeTemplate = '<input type="button" value="+" data-toggle="modal" data-target="#myModal" ng-click="GetItemsDetails(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.CategoriesGridData = pl;
        },
              function (errorPl) {
                 
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridItemsOption = {
            data: 'CategoriesGridData',
            jqueryUITheme: true,
            columnDefs: [
                           
                            {
                                displayName: "Item ID",
                                field: "ItemID",
                                width: 100,
                            },
                            
                            {
                                displayName: "Category ID",
                                field: "CategoryID",
                                width: 100,
                            },
                             {
                                 displayName: "Item Name",
                                 field: "ItemName",
                                 width: 100,
                             },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 100,
                            },
                            {
                                displayName: "Sub Items",
                                field: "SubitemDetails",
                                width:200,
                            },
                          
            ]
           
        };
    }
    //save Sub Items In Items
   

    //edit sub items

    $scope.editSubItems = function (ItemDetailsID) {
        alert(ItemDetailsID)
        var promiseGet = AccountService.editSubItems1(ItemDetailsID);
        // alert(promiseGet)
        //$scope.EditItemCode = promiseGet.ItemCode;
        promiseGet.then(function (p1) {
            $scope.EditItemCode = p1[0].ItemCode;
            $scope.EditSubItemName = p1[0].ItemSize;
            $scope.EditUOM = p1[0].UOM;
            $scope.EditTax = p1[0].TaxPrice;
            $scope.EditSP = p1[0].SalesPrice;
            $scope.EditStackUOM = p1[0].StackUOM;
            //$scope.Edit = p1[0].UOM;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
    }

    $scope.editItemForSubItem = function (ItemID) {
        $scope.ItemID = ItemID;
        alert($scope.ItemID)
        var promiseGet = AccountService.editItemForSubItem($scope.ItemID);
        promiseGet.then(function (p1) {
           // $scope.ItemGridData = p1.data;
            $scope.EditItemName = p1[0].ItemName;
            $scope.EditItemDescription = p1[0].ItemDescription;
            
            //$scope.Edit = p1[0].UOM;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
    }

    $scope.editItem = function (ItemID) {
        alert(ItemID)
        var promiseGet = AccountService.editItems1(ItemID);
        // alert(promiseGet)
        //$scope.EditItemCode = promiseGet.ItemCode;
        promiseGet.then(function (p1) {
            $scope.EditItemName = p1[0].ItemName;
            $scope.EditItemDescription = p1[0].ItemDescription;
         
            //$scope.Edit = p1[0].UOM;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });
    }


 

   
    //get LookUp Categories
    $scope.GetLookUpCategory = function () {
        var LookupCategoryID = $scope.LookupCategoryID;
       
        
        var promiseGet = AccountService.GetLookUpCategory();
        //alert(promiseGet); return true;
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModalEdit" ng-click="editlookupcategorys(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove"  ng-click="deleteLookUpCategory(row.entity)"  ng-click="updateLookUpCategoryDetails(row.entity)" /> ';
        promiseGet.then(function (pl) {
            $scope.LookupCategoryData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.LookupcategorygridOptions = {
            data: 'LookupCategoryData',
            jqueryUITheme: true,
            columnDefs: [
                           
                            {
                                displayName: "LookUpCategory Name",
                                field: "LookupCategoryName",
                                width: 150,
                            },
                            {
                                displayName: "LookUpCategory Code",
                                field: "LookupCategoryCode",
                                width: 150,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 150,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }
    
    //save lookupcategorys details
    $scope.saveLookupcategory = function () {
        alert($scope.model.LookupCategoryName);
        var LookUpCategoryViewModel = {
            LookupCategoryName : $scope.model.LookupCategoryName,
            LookupCategoryCode: $scope.model.LookupCategoryCode,
            LookupCategoryDescription: $scope.model.LookupCategoryDescription,
            IsActive: $scope.model.IsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveLookupcategory(LookUpCategoryViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearSectionDetails();
            $scope.GetSections();

        }, function (err) {
            alert("data not saved..");
            $scope.clearlookupcategoryDetails();
        });
    }


    $scope.clearlookupcategoryDetails = function () {
        $scope.model.LookupCategoryName = null;
        $scope.model.LookupCategoryCode = null;
        $scope.model.LookupCategoryDescription = null;
        $scope.IsActive = null;
    }

    //on Look Up Category Edit click
    $scope.editlookupcategorys = function (pl) {
        lookupCategoryID = pl.LookupCategoryID;
        $scope.EditLookupCategoryName = pl.LookupCategoryName;
        $scope.EditLookupCategoryCode = pl.LookupCategoryCode;
        $scope.EditLookupCategoryDescription = pl.LookupCategoryDescription;
        $scope.EditIsActive = pl.IsActive;
    }

    //Update LookUp Category details
    $scope.updateLookUpCategoryDetails = function () {
        alert('2')
       // alert($scope.EditLookupCategoryName)
        var lookUpCategoryViewModel = {
            LookupCategoryID: lookupCategoryID,
            LookupCategoryName: $scope.EditLookupCategoryName,
            LookupCategoryCode: $scope.EditLookupCategoryCode,
           // LookupCategoryDescription: $scope.EditLookupCategoryDescription,
            IsActive: $scope.EditIsActive,
            //CreatedBy: 1,
           // CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.updateLookUpCategoryDetails(lookUpCategoryViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearEditRestaurantDetails();
            $scope.GetLookUpCategory();

        }, function (err) {
            alert("data not saved..");
            $scope.clearEditRestaurantDetails();
        });
        
    };

    //Delete  LookUpCategory details
    $scope.deleteLookUpCategory = function (data) {
        var LookUpCategory = {
            LookupCategoryID: parseInt(data.LookupCategoryID),
            IsActive: false,
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.deleteLookUpCategory(LookUpCategory);
        promisePost.then(function (pl) {
            alert("Are you absolutely sure you want to delete?");
            $scope.GetLookUpCategory();

        }, function (err) {
            console.log("Err" + err);
        });
    };

    //save lookup details
    $scope.saveLookup = function () {
        //alert($scope.model.LookupCategoryName);
        var LookUpViewModel = {
            LookupCategoryID:$scope.LookupCategoryID,
            LookupName: $scope.model.LookupName,
            LookupCode: $scope.model.LookupCode,
            LookupDescription: $scope.model.LookupDescription,
            IsActive: $scope.model.IsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveLookup(LookUpViewModel);
        promisePost.then(function (pl) {

            alert("data saved..");
            $scope.clearSectionDetails();
            $scope.GetSections();

        }, function (err) {
            alert("data not saved..");
            $scope.clearlookupcategoryDetails();
        });
    }

    //get LookUp 
    $scope.RetriveLookupdata = function () {
        var LookupID = $scope.LookupID;
        var LookupCategoryID = $scope.LookupCategoryID
        var promiseGet = AccountService.RetriveLookupdata(LookupCategoryID);
        //alert(promiseGet); return true;
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModalEdit" ng-click="editlookup(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove"   ng-click="deleteLookUpDetails(row.entity)"  ng-click="updateLookUpDetails(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.LookupData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.LookupgridOptions = {
            data: 'LookupData',
            jqueryUITheme: true,
            columnDefs: [

                            {
                                displayName: "LookUp Name",
                                field: "LookupName",
                                width: 150,
                            },
                             {
                                 displayName: "LookUp Id",
                                 field: "LookupID",
                                 width: 150,
                             },
                            {
                                displayName: "LookUp Code",
                                field: "LookupCode",
                                width: 150,
                            },
                            {
                                displayName: "Is Active",
                                field: "IsActive",
                                width: 150,
                            },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }

    //on Look Up  Edit click
    $scope.editlookup = function (pl) {
        LookupID = pl.LookupID;
        $scope.LookupName = pl.LookupName;
        $scope.LookupCode = pl.LookupCode;
        $scope.LookupDescription = pl.LookupDescription;
        $scope.IsActive = pl.IsActive;
    }
    //Update LookUp Category details
    $scope.updateLookUpDetails = function () {
        // alert($scope.EditLookupCategoryName)
        var lookUpViewModel = {
            LookupID: LookupID,
            LookupCategoryID:$scope.LookupCategoryID,
            LookupName: $scope.LookupName,
            LookupCode: $scope.LookupCode,
            LookupDescription: $scope.LookupDescription,
            IsActive: $scope.IsActive,
            //CreatedBy: 1,
            // CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.updateLookUpDetails(lookUpViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            $scope.clearEditLookupDetails();
            $scope.RetriveLookupdata();

        }, function (err) {
            alert("data not saved..");
            $scope.clearEditLookupDetails();
        });

    };

    $scope.deleteLookUpDetails = function (data) {
        var LookUpDetails = {
            LookupID: parseInt(data.LookupID),
            IsActive: false,
            LastEditBy: 1,
            LastEditOn: new Date()
        }
           
        var promisePost = AccountService.deleteLookUpDetails(LookUpDetails);
        promisePost.then(function (pl) {
            alert(" Are you absolutely sure you want to delete?");
            $scope.RetriveLookupdata();

        }, function (err) {
            console.log("Err" + err);
        });
    };

    $scope.GetCategoryCode = function () {
        var CategoryCode = $scope.model.LookupCategoryCode;
        alert(CategoryCode);
        var promiseGet = AccountService.GetCategoryCode(CategoryCode);
        promiseGet.then(function (pl) {
            CategoryCode = pl;
            if(pl==0)
            {
                alert("Doest Exist")
            }
            else {
                alert("Already Exists..........")
            }
        },
        
        function (err) {
            alert("error")
        });
    }
    //delete subitems
    $scope.DeleteSubItems = function (data) {
        $scope.ItemDetailsID=data
        var ItemDetails = {
            ItemDetailsID: parseInt($scope.ItemDetailsID),
            IsActive: false,
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.DeleteSubItems(ItemDetails);
        promisePost.then(function (pl) {
            alert(" Are you absolutely sure you want to delete?");
            $scope.itemdetaildata();

        }, function (err) {
            console.log("Err" + err);
        });
    };
    //delete Items
    $scope.DeleteItems = function (data) {
        $scope.ItemID = data
        var ItemDetails = {
            ItemID: parseInt($scope.ItemID),
            IsActive: false,
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.DeleteItems(ItemDetails);
        promisePost.then(function (pl) {
            alert(" Are you absolutely sure you want to delete?");
            $scope.itemdetaildata();

        }, function (err) {
            console.log("Err" + err);
        });
    };
    //Getting Single Grid
    $scope.GetItemGrid = function (data) {
        alert(data)
        $scope.ItemID = data;
        var promiseGet = AccountService.GetItemGrid($scope.ItemID);
        //var removeTemplate = '<input type="button" value="+" data-toggle="modal" data-target="#myModal" ng-click="GetItemsDetails(row.entity)" />';
        promiseGet.then(function (ResData) {
            $scope.EditItemName = ResData[0].ItemName;
            $scope.EditItemDescription = ResData[0].ItemDescription;
            $scope.EditItemCode = ResData[0].ItemCode;
            $scope.Categories = ResData;
        },
              function (errorPl) {

                  console.log("Some Error in Getting Records." + errorPl);
              });
    };

  //Update Items
    $scope.UpdateItems = function (data) {
        $scope.ItemID = data;
        var ItemViewModel = {
            ItemID: $scope.ItemID,
            CategoryID: $scope.CategoryID,
            ItemName: $scope.EditItemName,
            ItemDescription: $scope.EditItemDescription,
            IsActive:1,
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.UpdateItems(ItemViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
        }, function (err) {
            alert("data not saved..");
            $scope.clearEditLookupDetails();
        });

    };
    //save SubItemGrid
    $scope.saveSubItemsItemsGrid = function (data) {
        $scope.ItemID = data;
        var ItemViewModel = {
            ItemID: $scope.ItemID,
            ItemSize: $scope.EditSubItemName,
            ItemCode: $scope.EditItemCode,
            UOM: $scope.EditUOM,
            IsActive: true,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.saveSubItemsItemsGrid(ItemViewModel);
        promisePost.then(function (Res) {
            $scope.Categories = Res.data;
            alert("data saved..");
        }, function (err) {
            alert("data not saved..");
            $scope.clearsaveItems();
        });
    };
    //Get Sub Item Single
    $scope.GetSubItemGrid = function (data) {
        
        $scope.ItemID = data;
        var promiseGet = AccountService.GetSubItemGrid($scope.ItemID);
        //var removeTemplate = '<input type="button" value="+" data-toggle="modal" data-target="#myModal" ng-click="GetItemsDetails(row.entity)" />';
        promiseGet.then(function (ResData) {
            $scope.EditItemName = ResData[0].ItemName;
            $scope.EditItemDescription = ResData[0].ItemDescription;
            $scope.EditItemCode = ResData[0].SubitemDetails[0].ItemCode;
            $scope.EditSubItemName = ResData[0].SubitemDetails[0].ItemSize;

            $scope.Categories = ResData;
        },
              function (errorPl) {

                  console.log("Some Error in Getting Records." + errorPl);
              });
    };

    //Get Customers
    $scope.GetCustomers = function () {
     
        var promiseGet = AccountService.GetCustomers();
        var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModalEdit" ng-click="editCustomerDetails(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove" ng-click="deleteRestaurants(row.entity)"  ng-click="UpdateCustomerDetails(row.entity)" />';
        promiseGet.then(function (pl) {
            $scope.RestaurantsData = pl;
        },
              function (errorPl) {
                  console.log("Some Error in Getting Records." + errorPl);
              });

        $scope.gridOptions = {
            data: 'RestaurantsData',
            jqueryUITheme: true,
            pagingOptions: $scope.pagingOptions,
            enablePaging: true,
            showFooter: true,
            columnDefs: [
                            {
                                displayName: "Customer Name",
                                field: "CustomerName",
                                width: 200,
                            },
                            {
                                displayName: "Email",
                                field: "Email",
                                width: 200,
                            },
                            
                            {
                                displayName: "Address",
                                field: "Address1",
                                width: 100,
                            },
                             {
                                 displayName: "State",
                                 field: "State",
                                 width: 100,
                             },
                            { field: 'remove', displayName: 'Actions', cellTemplate: removeTemplate }
            ]

        };
    }
    //Edit Customer
    $scope.editCustomerDetails = function (pl) {
        CustomerID = pl.CustomerID;
        $scope.EditCustomerName = pl.CustomerName;
        $scope.EditEmail = pl.Email;
        $scope.EditPhone = pl.Phone;
        $scope.EditAddress1 = pl.Address1;
      //  $scope.EditAddresse = p1.Address2;
        $scope.EditCity = pl.City;
        $scope.EditCountry = pl.Country;
       // $scope.EditState = p1.State;
       // $scope.EditZIPCode = p1.ZIPCode;
        $scope.EditIsActive = pl.IsActive;
    }

    //Update Customer
    $scope.UpdateCustomerDetails = function () {
      
        var CustomerViewModel = {
            CustomerID: CustomerID,
            CustomerName: $scope.EditCustomerName,
            Email: $scope.EditEmail,
            Phone: $scope.EditPhone,
            City: $scope.EditCity,
           // State: $scope.EditState,
            Address1: $scope.EditAddress1,
           // Address2:$scope.EditAddress2,
            Country: $scope.EditCountry,
            //ZIPCode:$scope.EditZIPCode,
            IsActive: $scope.EditIsActive,
            CreatedBy: 1,
            CreatedOn: new Date(),
            LastEditBy: 1,
            LastEditOn: new Date()
        }
        var promisePost = AccountService.UpdateCustomerDetails(CustomerViewModel);
        promisePost.then(function (pl) {
            alert("data saved..");
            //$scope.clearEditRestaurantDetails();
        }, function (err) {
            alert("data not saved..");
            $scope.clearEditRestaurantDetails();
        });
    };
    




});