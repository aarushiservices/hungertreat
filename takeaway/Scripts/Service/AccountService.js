﻿AccountApp.service('AccountService', function ($http, $q) {

    //Get Single Records  
    this.get = function (CreatedBy) {
        return $http.get("/api/AccountAPI/");
    }

    //retrieve restaurant details
    this.GetRestaurants = function () {
        var deferred = $q.defer();

        $http.get(
            "/api/AccountAPI/GetRestaurants"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //retrieve restaurant names for dropdowns
    this.GetRestaurantNames = function () {
        var deferred = $q.defer();

        $http.get(
            "/api/AccountAPI/GetRestaurantNames"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //save Restaurant details
    this.saveRestaurantDetails = function (_RestaurantDetails) {        
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveRestaurantDetails",
            data: _RestaurantDetails
        });
        return request;
    }

    //update Restaurant details
    this.updateRestaurantDetails = function (_RestaurantDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/updateRestaurantDetails",
            data: _RestaurantDetails
        });
        return request;
    }

    //delete Restaurant details
    this.deleteRestaurantDetails = function (_RestaurantDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/removeRestaurantDetails",
            data: _RestaurantDetails
        });
        return request;
    }

    //retrieve school dates
    this.GetCuisines = function (_restaurantID) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetriveCuisines",
            data: _restaurantID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //save Restaurant details
    this.saveCuisineDetails = function (_CuisineDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveCuisineDetails",
            data: _CuisineDetail
        });
        return request;
    }

    //save Items
    this.saveItems = function (_Items) {
        
        //alert(_Items.CategoryID);
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveItems",
            data: _Items
        });
        
        return request;
    }


    this.updateCuisineDetails = function (_CuisineDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/updateCuisineDetails",
            data: _CuisineDetails
        });
        return request;
    }

    //Sections
    this.GetSections = function () {
        var deferred = $q.defer();

        $http.get(
            "/api/AccountAPI/GetSections"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    
    this.saveSectionDetails = function (_SectionDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveSectionDetails",
            data: _SectionDetails
        });
        return request;
    }

    //Category
    this.GetCategories = function (_SectionID) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetriveCategories",
            data: _SectionID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }


    this.saveCategoryDetails = function (_CategoryDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveCategoryDetails",
            data: _CategoryDetails
        });
        return request;
    }

    //retieveing cities
    this.GetCities = function () {
        var deferred = $q.defer();

        $http.get(
            "/api/AccountAPI/GetCities"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //retrieve school dates
    this.GetAreas = function (_cityName) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetrieveAreas",
            data: _cityName
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //retrieve school dates
    this.RetrieveRestaurantDetails = function (RestaurantDetailViewModel) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetrieveRestaurantDetails",
            data: RestaurantDetailViewModel
            
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }


    this.GetRestaurantMenu = function (_RestaurantID) {
        alert(_RestaurantID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetrieveRestaurantMenu",
            data: _RestaurantID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //retrive Items
    this.GetItems = function (_categoryID) {
        //alert(_categoryID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetriveItems",
            data: _categoryID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //retrieve school dates
    this.RetriveCategories = function (_SectionID) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetriveCategories",
            data: _restaurantID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //GetLookupCategories
    this.GetLookUpCategory = function () {
        //alert('hi i am here'); return true;
        var deferred = $q.defer();
        $http.get(
            "/api/AccountAPI/GetLookUpCategory"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
   
    //SaveLookuocategory
    this.saveLookupcategory = function (_LookupCategoryDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveLookupcategory",
            data: _LookupCategoryDetails
        });
        return request;
    }
    //update LookupCategories
    this.updateLookUpCategoryDetails = function (_categoriesdetails) {
       // alert(_categoriesdetails)
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/updateLookUpCategoryDetails",
            data: _categoriesdetails
        });
        return request;
    }
    //delete LookupCategories details
    this.deleteLookUpCategory = function (_Lookup) {
       // alert('s')
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/removeLookupcategories",
            data: _Lookup
        });
        return request;
    }
    //SaveLookup
    this.saveLookup = function (_Lookup) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveLookup",
            data: _Lookup
        });
        return request;
    }
    //GET LOOKUP
    //GetLookup
    this.RetriveLookupdata = function (_lookupcategorytID) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetriveLookupdata",
            data: _lookupcategorytID
        }).success(function (data) {           
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //update LookupCategories
    this.updateLookUpDetails = function (_lookupdetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/updateLookUpDetails",
            data: _lookupdetails
        });
        return request;
    }

    //delete Lookup details
    this.deleteLookUpDetails = function (_LookupDetele) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/removeLookup",
            data: _LookupDetele
        });
        return request;
    }
    //GetLookupCategories
    this.GetCategoryCode = function (_CategoryCode) {
       // alert(_CategoryCode)
        var deferred = $q.defer();
        $http.get(
            '/api/AccountAPI/RetriveCategoryCode', {
            params:{
                CategoryCode:_CategoryCode
            }
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });
        return deferred.promise;
    }


    this.editSubItems1 = function (_ItemDetailsID) {
      //  alert(_ItemDetailsID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/editSubItems1",
            data: _ItemDetailsID
        }).success(function (data) {
           // alert('1')
            deferred.resolve(data);
        }).error(function (data) {
           // alert('2')
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.editItemForSubItem = function (_ItemDetailsID) {
       // alert(_ItemDetailsID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/editItems1",
            data: _ItemDetailsID
        }).success(function (data) {
            //alert('1')
            deferred.resolve(data);
        }).error(function (data) {
           // alert('2')
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.editItems1 = function (_ItemDetailsID) {
        alert(_ItemDetailsID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/editItems1",
            data: _ItemDetailsID
        }).success(function (data) {
           // alert('1')
            deferred.resolve(data);
        }).error(function (data) {
           // alert('2')
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //delete LookupCategories details
    this.DeleteSubItems = function (_DetailsId) {  
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/removesubitemDetails",
            data: _DetailsId
        });
        return request;
    }

    //delete Items

    this.DeleteItems = function (_ItemID) { 
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/removeitemDetails",
            data: _ItemID
        });
        return request;
    }

    //SaveLookuocategory
    this.saveSubItemsItems = function (_Subitemdetails) {
       // alert('1')
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveSubItemsItems",
            data: _Subitemdetails
        });
        return request;
    }


    this.GetItemsByID = function (_categoryID) {
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/RetriveItems",
            data: _categoryID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.GetItemGrid = function (_categoryID) {
        alert(_categoryID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/ReriveItemGid",
            data: _categoryID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //update Item Details
    this.UpdateItems = function (_ItemDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/UpdateItems",
            data: _ItemDetails
        });
        return request;
    }

   

    this.saveSubItemsItemsGrid = function (_Subitemdetails) {
        alert('1')
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/saveSubItemsItems",
            data: _Subitemdetails
        });
        return request;
    }

    this.GetSubItemGrid = function (_categoryID) {
        alert(_categoryID)
        var deferred = $q.defer();
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/ReriveSubItemGid",
            data: _categoryID
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //retrieve Customers details
    this.GetCustomers = function () {
        var deferred = $q.defer();
        $http.get(
            "/api/AccountAPI/GetCustomers"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    //update Customer details
    this.UpdateCustomerDetails = function (_CustomerDetails) {
        var request = $http({
            method: "post",
            url: "/api/AccountAPI/UpdateCustomerDetails",
            data: _CustomerDetails
        });
        return request;
    }

   
});