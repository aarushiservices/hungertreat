﻿using takeaway.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Http;
using takeaway.Entities;

namespace takeaway.WebAPI
{
    public class AccountAPIController : ApiController
    {
        private SqlConnection _sqlConnection;
        private bool _isRestaurantDetailsSaved;
        private bool _isCuisineDetailsSaved;
        private bool _isSectionDetailsSaved;
        private bool _isCategoryDetailsSaved;
        private foodcourtEntities _foodcourtEntities;
        private int _restaurantID;
        private int _sectionID;
        private int _categoryID;
        private int _itemID;
        private bool _isItemsSaved;
        private bool _isLookupcatgorySaved;
        private bool _isLookupSaved;
        private bool _isCustomerDetailsSaved;

        public AccountAPIController()
        {
            _foodcourtEntities = new foodcourtEntities();
        }
        private void CreateConnection()
        {
            string _sqlConnectionString =
            ConfigurationManager.ConnectionStrings["foodcourtConnection"].ConnectionString;
            _sqlConnection = new SqlConnection(_sqlConnectionString);
        }
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        #region Restaurants

        //GET api/GetRestaurants
        public IEnumerable<RestaurantViewModel> GetRestaurants()
        {
            IEnumerable<RestaurantViewModel> restaurantsList;
            try
            {
                List<RestaurantViewModel> _restaurantViewModelList = new List<RestaurantViewModel>();
                string oString = "Select * from tbl_Restaurant where IsActive=1";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        RestaurantViewModel _restaurantViewModel = new RestaurantViewModel();
                        _restaurantViewModel.RestaurantID = Convert.ToInt32(oReader["RestaurantID"]);
                        _restaurantViewModel.RestaurantName = oReader["RestaurantName"].ToString();
                        _restaurantViewModel.RestaurantShortName = oReader["RestaurantShortName"].ToString();
                        _restaurantViewModel.RestaurantDescription = oReader["RestaurantDescription"].ToString();
                        _restaurantViewModel.TAXID = oReader["TAXID"].ToString();
                        _restaurantViewModel.PrimaryContactName = oReader["PrimaryContactName"].ToString();
                        _restaurantViewModel.PrimaryContactEmail = oReader["PrimaryContactEmail"].ToString();
                        _restaurantViewModel.PrimaryContactNumber = oReader["PrimaryContactNumber"].ToString();
                        _restaurantViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _restaurantViewModel.CreatedBy = Convert.ToInt32(oReader["CreatedBy"]);
                        _restaurantViewModel.LastEditBy = Convert.ToInt32(oReader["LastEditBy"]);
                        _restaurantViewModelList.Add(_restaurantViewModel);
                    }

                    _sqlConnection.Close();
                }
                restaurantsList = _restaurantViewModelList;
                return restaurantsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //POST api/saveRestaurantDetails
        public bool saveRestaurantDetails(RestaurantViewModel _RestaurantViewModel)
        {
            try
            {
                tbl_Restaurant _restaurantViewModel = new tbl_Restaurant();
                _restaurantViewModel.RestaurantName = _RestaurantViewModel.RestaurantName;
                _restaurantViewModel.RestaurantDescription = _RestaurantViewModel.RestaurantDescription;
                _restaurantViewModel.RestaurantShortName = _RestaurantViewModel.RestaurantShortName;
                _restaurantViewModel.TAXID = _RestaurantViewModel.TAXID;
                _restaurantViewModel.PrimaryContactName = _RestaurantViewModel.PrimaryContactName;
                _restaurantViewModel.PrimaryContactEmail = _RestaurantViewModel.PrimaryContactEmail;
                _restaurantViewModel.PrimaryContactNumber = _RestaurantViewModel.PrimaryContactNumber;
                _restaurantViewModel.IsActive = _RestaurantViewModel.IsActive;
                _restaurantViewModel.CreatedBy = _RestaurantViewModel.CreatedBy;
                _restaurantViewModel.CreatedOn = _RestaurantViewModel.CreatedOn;
                _restaurantViewModel.LastEditBy = _RestaurantViewModel.LastEditBy;
                _restaurantViewModel.LastEditOn = _RestaurantViewModel.LastEditOn;
                _foodcourtEntities.tbl_Restaurant.Add(_restaurantViewModel);
                _foodcourtEntities.SaveChanges();
                return _isRestaurantDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        //POST api/updateRestaurantDetails
        public bool updateRestaurantDetails(RestaurantViewModel _RestaurantViewModel)
        {
            try
            {
                tbl_Restaurant _restaurantViewModel = new tbl_Restaurant();
                _restaurantViewModel = (from a in _foodcourtEntities.tbl_Restaurant where a.RestaurantID == _RestaurantViewModel.RestaurantID select a).FirstOrDefault();
                _restaurantViewModel.RestaurantName = _RestaurantViewModel.RestaurantName;
                _restaurantViewModel.RestaurantDescription = _RestaurantViewModel.RestaurantDescription;
                _restaurantViewModel.RestaurantShortName = _RestaurantViewModel.RestaurantShortName;
                _restaurantViewModel.TAXID = _RestaurantViewModel.TAXID;
                _restaurantViewModel.PrimaryContactName = _RestaurantViewModel.PrimaryContactName;
                _restaurantViewModel.PrimaryContactEmail = _RestaurantViewModel.PrimaryContactEmail;
                _restaurantViewModel.PrimaryContactNumber = _RestaurantViewModel.PrimaryContactNumber;
                _restaurantViewModel.IsActive = _RestaurantViewModel.IsActive;
                _restaurantViewModel.CreatedBy = _RestaurantViewModel.CreatedBy;
                _restaurantViewModel.CreatedOn = _RestaurantViewModel.CreatedOn;
                _restaurantViewModel.LastEditBy = _RestaurantViewModel.LastEditBy;
                _restaurantViewModel.LastEditOn = _RestaurantViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isRestaurantDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        //POST api/deleteRestaurantDetails
        public bool removeRestaurantDetails(RestaurantViewModel _RestaurantViewModel)
        {
            try
            {
                tbl_Restaurant _restaurantViewModel = new tbl_Restaurant();
                _restaurantViewModel = (from a in _foodcourtEntities.tbl_Restaurant where a.RestaurantID == _RestaurantViewModel.RestaurantID select a).FirstOrDefault();
                _restaurantViewModel.IsActive = _RestaurantViewModel.IsActive;
                _restaurantViewModel.LastEditBy = _RestaurantViewModel.LastEditBy;
                _restaurantViewModel.LastEditOn = _RestaurantViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isRestaurantDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        #endregion Restaurants

        #region GetRestaurantNames

        //GET api/GetRestaurants
        public IEnumerable<RestaurantViewModel> GetRestaurantNames()
        {
            IEnumerable<RestaurantViewModel> restaurantsList;
            try
            {
                restaurantsList = _foodcourtEntities.tbl_Restaurant.Select(a =>
                    new RestaurantViewModel
                    {
                        RestaurantID = a.RestaurantID,
                        RestaurantName = a.RestaurantName,
                        RestaurantShortName = a.RestaurantShortName
                    });
                return restaurantsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        #endregion GetRestaurantNames

        #region Cuisines
        //GET api/GetCuisines
        public IEnumerable<CuisineViewModel> RetriveCuisines(object _restaurantID)
        {
            IEnumerable<CuisineViewModel> cuisinesList;
            int restaurantID= Convert.ToInt32(_restaurantID);
            try
            {
                cuisinesList = _foodcourtEntities.tbl_Cuisine.Where(b => b.RestaurantID == restaurantID) 
                .Select(a =>
                   new CuisineViewModel
                   {
                       RestaurantID = a.RestaurantID,
                       CuisineID = a.CuisineID,
                       CuisineType = a.CuisineType,
                       IsActive = a.IsActive,
                       CreatedBy = a.CreatedBy,
                       CreatedOn = a.CreatedOn,
                       LastEditBy = a.LastEditBy,
                       LastEditOn = a.LastEditOn
                   });
                return cuisinesList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //POST api/saveCuisineDetails
        public bool saveCuisineDetails(CuisineViewModel _CuisineViewModel)
        {
            try
            {
                tbl_Cuisine _cuisineViewModel = new tbl_Cuisine();
                _cuisineViewModel.RestaurantID = _CuisineViewModel.RestaurantID;
                _cuisineViewModel.CuisineType = _CuisineViewModel.CuisineType;
                _cuisineViewModel.IsActive = _CuisineViewModel.IsActive;
                _cuisineViewModel.CreatedBy = _CuisineViewModel.CreatedBy;
                _cuisineViewModel.CreatedOn = _CuisineViewModel.CreatedOn;
                _cuisineViewModel.LastEditBy = _CuisineViewModel.LastEditBy;
                _cuisineViewModel.LastEditOn = _CuisineViewModel.LastEditOn;
                _foodcourtEntities.tbl_Cuisine.Add(_cuisineViewModel);
                _foodcourtEntities.SaveChanges();
                return _isCuisineDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        public bool updateCuisineDetails(CuisineViewModel _CuisineViewModel)
        {
            try
            {
                tbl_Cuisine _cuisineViewModel = new tbl_Cuisine();
                _cuisineViewModel = (from a in _foodcourtEntities.tbl_Cuisine where a.CuisineID == _CuisineViewModel.CuisineID select a).FirstOrDefault();
                _cuisineViewModel.CuisineType = _CuisineViewModel.CuisineType;
                _cuisineViewModel.IsActive = _CuisineViewModel.IsActive;
                //_cuisineViewModel.CreatedBy = _CuisineViewModel.CreatedBy;
                //_cuisineViewModel.CreatedOn = _CuisineViewModel.CreatedOn;
                _cuisineViewModel.LastEditBy = _CuisineViewModel.LastEditBy;
                _cuisineViewModel.LastEditOn = _CuisineViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isCuisineDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }
        #endregion Cuisines

        #region Sections
        //GET api/GetSections
        public IEnumerable<SectionViewModel> GetSections()
        {
            IEnumerable<SectionViewModel> sectionsList;
            try
            {
                List<SectionViewModel> _sectionViewModelList = new List<SectionViewModel>();
                string oString = "Select * from tbl_Section where restaurantid=1";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        SectionViewModel _sectionViewModel = new SectionViewModel();
                        _sectionViewModel.SectionID = Convert.ToInt32(oReader["SectionID"]);
                        _sectionViewModel.RestaurantID = Convert.ToInt32(oReader["RestaurantID"]);
                        _sectionViewModel.SectionName = oReader["SectionName"].ToString();
                        _sectionViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _sectionViewModel.CreatedBy = Convert.ToInt32(oReader["CreatedBy"]);
                        _sectionViewModel.LastEditBy = Convert.ToInt32(oReader["LastEditBy"]);
                        _sectionViewModelList.Add(_sectionViewModel);
                    }

                    _sqlConnection.Close();
                }
                sectionsList = _sectionViewModelList;
                return sectionsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //POST api/saveSectionDetails
        public bool saveSectionDetails(SectionViewModel _SectionViewModel)
        {
            try
            {
                tbl_Section _sectionViewModel = new tbl_Section();
                _sectionViewModel.RestaurantID = _SectionViewModel.RestaurantID;
                _sectionViewModel.SectionName= _SectionViewModel.SectionName;
                _sectionViewModel.IsActive = _SectionViewModel.IsActive;
                _sectionViewModel.CreatedBy = _SectionViewModel.CreatedBy;
                _sectionViewModel.CreatedOn = _SectionViewModel.CreatedOn;
                _sectionViewModel.LastEditBy = _SectionViewModel.LastEditBy;
                _sectionViewModel.LastEditOn = _SectionViewModel.LastEditOn;
                _foodcourtEntities.tbl_Section.Add(_sectionViewModel);
                _foodcourtEntities.SaveChanges();
                return _isSectionDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        #endregion Sections

        #region Categories
        //GET api/GetCategories
        public IEnumerable<CategoryViewModel> GetCategories(object _SectionID)
        {
            
            IEnumerable<CategoryViewModel> categoriesList;
            int CategoryID = Convert.ToInt32(_SectionID);
            try
            {
                List<CategoryViewModel> _categoryViewModelList = new List<CategoryViewModel>();
                string oString = "select tc.*,ts.sectionname from tbl_Category tc, tbl_Section ts where tc.SectionID = ts.SectionID and ts.RestaurantID=1";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        CategoryViewModel _categoryViewModel = new CategoryViewModel();
                        _categoryViewModel.CategoryID = Convert.ToInt32(oReader["CategoryID"]);
                        _categoryViewModel.SectionID = Convert.ToInt32(oReader["SectionID"]);
                        _categoryViewModel.SectionName = oReader["SectionName"].ToString();
                        _categoryViewModel.CategoryName = oReader["CategoryName"].ToString();
                        _categoryViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _categoryViewModel.CreatedBy = Convert.ToInt32(oReader["CreatedBy"]);
                        _categoryViewModel.LastEditBy = Convert.ToInt32(oReader["LastEditBy"]);
                        _categoryViewModelList.Add(_categoryViewModel);
                    }

                    _sqlConnection.Close();
                }
                categoriesList = _categoryViewModelList;
                return categoriesList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //POST api/saveCategoryDetails
        public bool saveCategoryDetails(CategoryViewModel _CategoryViewModel)
        {
            try
            {
                tbl_Category _categoryViewModel = new tbl_Category();
                _categoryViewModel.CategoryID = _CategoryViewModel.CategoryID;
                _categoryViewModel.SectionID = _CategoryViewModel.SectionID;
                _categoryViewModel.CategoryName = _CategoryViewModel.CategoryName;
                _categoryViewModel.IsActive = _CategoryViewModel.IsActive;
                _categoryViewModel.CreatedBy = _CategoryViewModel.CreatedBy;
                _categoryViewModel.CreatedOn = _CategoryViewModel.CreatedOn;
                _categoryViewModel.LastEditBy = _CategoryViewModel.LastEditBy;
                _categoryViewModel.LastEditOn = _CategoryViewModel.LastEditOn;
                _foodcourtEntities.tbl_Category.Add(_categoryViewModel);
                _foodcourtEntities.SaveChanges();
                return _isSectionDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        #endregion Categories

        

        //GET api/GetRestaurants
        public IEnumerable<RestaurantDetailViewModel> GetCities()
        {
            IEnumerable<RestaurantDetailViewModel> restaurantsList;
            try
            {
                restaurantsList = _foodcourtEntities.tbl_RestaurantDetails.Where(a => a.IsActive == true).Select(a =>
                    new RestaurantDetailViewModel
                    {
                        //RestaurantID = a.RestaurantID,
                        //Address1 = a.Address1,
                        //Address2 = a.Address2,
                        City = a.City,
                        //State = a.State,
                        //Country = a.Country,
                        //ZIPCode = a.ZIPCode,
                       // IsActive = a.IsActive,
                       // CreatedBy = a.CreatedBy,
                       // CreatedOn = a.CreatedOn,
                       // LastEditBy = a.LastEditBy,
                       // LastEditOn = a.LastEditOn,
                    }).Distinct();
                return restaurantsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //GET api/GetRestaurants
        public IEnumerable<RestaurantDetailViewModel> RetrieveAreas(RestaurantDetailViewModel _cityName)
        {
            IEnumerable<RestaurantDetailViewModel> restaurantsList;
            string cityName = _cityName.City.ToString();
            try
            {
                restaurantsList = _foodcourtEntities.tbl_RestaurantDetails.Where(a => a.IsActive == true && a.City == cityName).Select(a =>
                    new RestaurantDetailViewModel
                    {
                        //RestaurantID = a.RestaurantID,
                        //Address1 = a.Address1,
                        Address2 = a.Address2,
                        //City = a.City,
                        //State = a.State,
                        //Country = a.Country,
                        //ZIPCode = a.ZIPCode,
                        // IsActive = a.IsActive,
                        // CreatedBy = a.CreatedBy,
                        // CreatedOn = a.CreatedOn,
                        // LastEditBy = a.LastEditBy,
                        // LastEditOn = a.LastEditOn,
                    }).Distinct();
                return restaurantsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //GET api/GetRestaurantImage
        public List<RestaurantDetailViewModel> RetrieveRestaurantDetails(RestaurantDetailViewModel _restaurantDetailViewModel)
        {
            List<RestaurantDetailViewModel> restaurantsList = null;
            try
            {
                if (_foodcourtEntities.tbl_RestaurantDetails.Any(a => a.City.Contains(_restaurantDetailViewModel.ZIPCode) || a.Address2.Contains(_restaurantDetailViewModel.ZIPCode) || a.ZIPCode.Contains(_restaurantDetailViewModel.ZIPCode)))
                {
                   // _restaurantID = _foodcourtEntities.tbl_RestaurantDetails.Where(a => a.City.Contains(_restaurantDetailViewModel.ZIPCode) || a.Address2.Contains(_restaurantDetailViewModel.ZIPCode) || a.ZIPCode.Contains(_restaurantDetailViewModel.ZIPCode)).Select(a => a.RestaurantID).FirstOrDefault();
                    //if(_foodcourtEntities.tbl_RestaurantImage.Any(a => a.RestaurantID == _restaurantID))
                    //{
                        restaurantsList = _foodcourtEntities.tbl_RestaurantDetails
                            .Join(_foodcourtEntities.tbl_RestaurantImage, rd => rd.RestaurantID, ri => ri.RestaurantID, (rd, ri) => new { rd, ri })
                            .Join(_foodcourtEntities.tbl_Restaurant, rdi => rdi.rd.RestaurantID, re => re.RestaurantID, (rdi, re) => new { rdi,re})
                            .Where(a => a.rdi.rd.City.Contains(_restaurantDetailViewModel.ZIPCode) || a.rdi.rd.Address2.Contains(_restaurantDetailViewModel.ZIPCode) || a.rdi.rd.ZIPCode.Contains(_restaurantDetailViewModel.ZIPCode)).Select(a =>
                        //restaurantsList = _foodcourtEntities.tbl_RestaurantImage.Where(a => a.RestaurantID == 1).Select(a => 
                            new RestaurantDetailViewModel
                            {
                                //RestaurantImage = a.RestaurantImage
                                RestaurantImage = a.rdi.ri.RestaurantImage,
                                RestaurantName =  a.re.RestaurantName,// + "\r\n" + a.rdi.rd.Address1 + "," + a.rdi.rd.Address2 + "," + a.rdi.rd.City + "," + a.rdi.rd.State + "," + a.rdi.rd.Country,
                                RestaurantID = a.re.RestaurantID,
                                Address1 = a.rdi.rd.Address1+","+a.rdi.rd.Address2+","+a.rdi.rd.City+","+a.rdi.rd.State+","+a.rdi.rd.Country,
                                //City = a.City,
                                //State = a.State,
                                //Country = a.Country,
                                //ZIPCode = a.ZIPCode,
                                // IsActive = a.IsActive,
                                // CreatedBy = a.CreatedBy,
                                // CreatedOn = a.CreatedOn,
                                // LastEditBy = a.LastEditBy,
                                // LastEditOn = a.LastEditOn,
                            }).Distinct().ToList();
                   // }
                        
                }
               
                
                return restaurantsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

       // GET api/GetRestaurantImage
        //public List<SectionViewModel> RetrieveRestaurantMenu(object _restaurantID)
        //{
        //    List<SectionViewModel> restaurantsList = null;
        //    int restaurantID = Convert.ToInt32(_restaurantID);
        //    try
        //    {
        //        if (_foodcourtEntities.tbl_Section.Any(a => a.RestaurantID == restaurantID))
        //        {

        //            restaurantsList = _foodcourtEntities.tbl_Section.Where(a => a.RestaurantID == restaurantID).Select(a =>
        //                    new SectionViewModel
        //                    {
        //                        SectionID = a.SectionID,
        //                        SectionName = a.SectionName,
        //                        categories = _foodcourtEntities.tbl_Category.Where(b => b.SectionID == a.SectionID).Select(c =>
        //                        new CategoryViewModel
        //                        {
        //                            CategoryID = c.CategoryID,
        //                            CategoryName = c.CategoryName,
        //                            items = _foodcourtEntities.tbl_Item.Where(d => d.CategoryID == c.CategoryID).Select(e =>
        //                            new ItemViewModel
        //                            {
        //                                ItemID = e.ItemID,
        //                                ItemName = e.ItemName,
        //                                ItemDescription = e.ItemDescription,
        //                                ItemImage = e.ItemImage,
        //                                itemsDetails = _foodcourtEntities.tbl_ItemDetails.Where(f => f.ItemID == e.ItemID).Select(g => 
        //                                new ItemDetailsViewModel
        //                                {
        //                                    ItemCode = g.ItemCode,
        //                                    ItemDetailsID = g.ItemDetailsID,
        //                                    ItemSize = g.ItemSize
        //                                }).ToList()
        //                            }).ToList()
        //                        }).ToList()
        //                    }).ToList();
        //            }
        //        return restaurantsList;
        //    }
        //    catch (Exception ex)
        //    {
        //       // log4net.ThreadContext.Properties["district"] = _districtID;
        //       // log4net.ThreadContext.Properties["userid"] = _userid;
        //       // _iloggingservice.Error("Error while retrieving school dates.", ex);
        //        throw ex;
        //    }

        //}

        public List<RestaurantMenuViewModel> RetrieveRestaurantMenu(object _restaurantID)
        {
            List<RestaurantMenuViewModel> restaurantsList = null;
            int restaurantID = Convert.ToInt32(_restaurantID);
            try
            {
                if (_foodcourtEntities.tbl_Section.Any(a => a.RestaurantID == restaurantID))
                {

                    restaurantsList = _foodcourtEntities.tbl_Section
                                        .Join(_foodcourtEntities.tbl_Category, ts => ts.SectionID, tc => tc.SectionID, (ts, tc) => new { ts, tc })
                                        .Join(_foodcourtEntities.tbl_Item, tsc => tsc.tc.CategoryID, ti => ti.CategoryID, (tsc, ti) => new { tsc, ti })
                                        .Join(_foodcourtEntities.tbl_ItemDetails, tsci => tsci.ti.ItemID, tid => tid.ItemID, (tsci, tid) => new { tsci, tid })
                                        .Where(a => a.tsci.tsc.ts.RestaurantID == restaurantID)
                                        .Select(b =>
                                        new RestaurantMenuViewModel
                                        {
                                            SectionID = b.tsci.tsc.ts.SectionID,
                                            SectionName = b.tsci.tsc.ts.SectionName,
                                            CategoryID = b.tsci.tsc.tc.CategoryID,
                                            CategoryName = b.tsci.tsc.tc.CategoryName,
                                            ItemID = b.tsci.ti.ItemID,
                                            ItemName = b.tsci.ti.ItemName,
                                            ItemDescription = b.tsci.ti.ItemDescription,
                                            ItemImage = b.tsci.ti.ItemImage,
                                            ItemDetailsID = b.tid.ItemDetailsID,
                                            ItemCode = "Code: "+ b.tid.ItemCode,
                                            ItemSize = "Item Size: "+b.tid.ItemSize,
                                            SalesPrice = "Sales Price: " + b.tid.SalesPrice,
                                            TaxPrice = "Tax Price:" + b.tid.TaxPrice,
                                            Discount = "Discount:" + b.tid.Discount,
                                            UOM ="Unit of Measure:" + b.tid.UOM,
                                            Quantity = 0
                                        }).ToList();
                    //restaurantsList = _foodcourtEntities.tbl_Section.Where(a => a.RestaurantID == restaurantID).Select(a =>
                    //        new SectionViewModel
                    //        {
                    //            SectionID = a.SectionID,
                    //            SectionName = a.SectionName,
                    //            categories = _foodcourtEntities.tbl_Category.Where(b => b.SectionID == a.SectionID).Select(c =>
                    //            new CategoryViewModel
                    //            {
                    //                CategoryID = c.CategoryID,
                    //                CategoryName = c.CategoryName,
                    //                items = _foodcourtEntities.tbl_Item.Where(d => d.CategoryID == c.CategoryID).Select(e =>
                    //                new ItemViewModel
                    //                {
                    //                    ItemID = e.ItemID,
                    //                    ItemName = e.ItemName,
                    //                    ItemDescription = e.ItemDescription,
                    //                    ItemImage = e.ItemImage,
                    //                    itemsDetails = _foodcourtEntities.tbl_ItemDetails.Where(f => f.ItemID == e.ItemID).Select(g => 
                    //                    new ItemDetailsViewModel
                    //                    {
                    //                        ItemCode = g.ItemCode,
                    //                        ItemDetailsID = g.ItemDetailsID,
                    //                        ItemSize = g.ItemSize
                    //                    }).ToList()
                    //                }).ToList()
                    //            }).ToList()
                    //        }).ToList();
                }
                return restaurantsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }



        //Get Items
        public IEnumerable<ItemViewModel> Getitems1()
        {
            IEnumerable<ItemViewModel> ItemsList;
            try
            {
                List<ItemViewModel> _ItemViewModelList = new List<ItemViewModel>();
                string oString = "Select * from tbl_Item";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        ItemViewModel _ItemViewModel = new ItemViewModel();
                        _ItemViewModel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                       // _ItemViewModel.CategoryID = Convert.ToInt32(oReader["CategoryID"]);
                        _ItemViewModel.ItemName = oReader["ItemName"].ToString();
                        _ItemViewModel.ItemDescription = oReader["ItemDescription"].ToString();
                        _ItemViewModel.ItemImage = oReader["ItemImage"].ToString();
                        _ItemViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _ItemViewModel.CreatedBy = Convert.ToInt32(oReader["CreatedBy"]);
                       // _ItemViewModel.LastEditBy = Convert.ToInt32(oReader["LastEditBy"]);
                        _ItemViewModelList.Add(_ItemViewModel);
                    }

                    _sqlConnection.Close();
                }
                ItemsList = _ItemViewModelList;
                return ItemsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //save Items

        public List<ItemViewModel> saveItems(ItemViewModel ItemViewModel)
        {
            List<ItemViewModel> itemsList = new List<ItemViewModel>();
            try
            {               
                tbl_Item _itemViewModel = new tbl_Item();
                _itemViewModel.CategoryID = ItemViewModel.CategoryID;
                _itemViewModel.ItemName = ItemViewModel.ItemName;
                _itemViewModel.ItemDescription = ItemViewModel.ItemDescription;
                _itemViewModel.ItemImage = "img";//ItemViewModel.ItemImage;
                _itemViewModel.IsActive = ItemViewModel.IsActive;
                _itemViewModel.CreatedBy = ItemViewModel.CreatedBy;
                _itemViewModel.CreatedOn = ItemViewModel.CreatedOn;
                _itemViewModel.LastEditBy = ItemViewModel.LastEditBy;
                _itemViewModel.LastEditOn = ItemViewModel.LastEditOn;
                _foodcourtEntities.tbl_Item.Add(_itemViewModel);
                var data = _foodcourtEntities.SaveChanges();
                if (data == 1)
                {
                    string oString = "select ItemID,ItemName,ItemDescription,ItemImage,IsActive from tbl_Item where IsActive=1 and ItemID=" + Convert.ToString(_itemViewModel.ItemID);
                    CreateConnection();
                    SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                    _sqlConnection.Open();
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            CreateConnection();
                            string osubitemString = "select ItemID,ItemDetailsID,ItemCode,ItemSize,SalesPrice from tbl_ItemDetails where ItemID=" + Convert.ToString(oReader["ItemID"]); ;
                            SqlCommand osubitemCmd = new SqlCommand(osubitemString, _sqlConnection);
                            _sqlConnection.Open();
                            List<ItemDetailsViewModel> itemsDetailsList = new List<ItemDetailsViewModel>();
                            using (SqlDataReader osubitemReader = osubitemCmd.ExecuteReader())
                            {
                                while (osubitemReader.Read())
                                {
                                    ItemDetailsViewModel _itemsDetailsviewmodel = new ItemDetailsViewModel();
                                    _itemsDetailsviewmodel.ItemDetailsID = Convert.ToInt32(osubitemReader["ItemDetailsID"]);
                                    _itemsDetailsviewmodel.ItemID = Convert.ToInt32(osubitemReader["ItemID"]);
                                    _itemsDetailsviewmodel.ItemCode = Convert.ToString(osubitemReader["ItemCode"]);
                                    _itemsDetailsviewmodel.ItemSize = Convert.ToString(osubitemReader["ItemSize"]);
                                    _itemsDetailsviewmodel.SalesPrice = Convert.ToString(osubitemReader["SalesPrice"]);
                                    itemsDetailsList.Add(_itemsDetailsviewmodel);
                                }
                            }
                            ItemViewModel _itemsviewmodel = new ItemViewModel();
                            _itemsviewmodel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                            _itemsviewmodel.ItemName = oReader["ItemName"].ToString();
                            _itemsviewmodel.ItemDescription = oReader["ItemDescription"].ToString();
                            _itemsviewmodel.ItemImage = oReader["ItemImage"].ToString();
                            _itemsviewmodel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                            _itemsviewmodel.SubitemDetails = itemsDetailsList;
                            itemsList.Add(_itemsviewmodel);
                        }
                        _sqlConnection.Close();
                    }
                }
                // int ItemID = _itemViewModel.ItemID;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);                
            }
            return itemsList;
        }
        //save sub Items
        public List<ItemViewModel> saveSubItemsItems(ItemDetailsViewModel ItemDetailsViewModel)
        {
            List<ItemViewModel> itemsList = new List<ItemViewModel>();
            try
            {
                tbl_ItemDetails _itemViewModel = new tbl_ItemDetails();
                _itemViewModel.ItemCode = ItemDetailsViewModel.ItemCode;
                _itemViewModel.ItemID = ItemDetailsViewModel.ItemID;
                _itemViewModel.ItemSize = ItemDetailsViewModel.ItemSize;
                _itemViewModel.UOM = ItemDetailsViewModel.UOM;
                _itemViewModel.SalesPrice = "10";
                _itemViewModel.TaxPrice = "10";
                _itemViewModel.Discount = "10";
                _itemViewModel.CostPrice = "10";
                _itemViewModel.MBQ = "10";
                _itemViewModel.StackUOM = "10";
                _itemViewModel.IsActive = ItemDetailsViewModel.IsActive;
                _itemViewModel.CreatedBy = ItemDetailsViewModel.CreatedBy;
                _itemViewModel.CreatedOn = ItemDetailsViewModel.CreatedOn;
                _itemViewModel.LastEditBy = ItemDetailsViewModel.LastEditBy;
                _itemViewModel.LastEditOn = ItemDetailsViewModel.LastEditOn;
                _foodcourtEntities.tbl_ItemDetails.Add(_itemViewModel);
                var data = _foodcourtEntities.SaveChanges();
                if (data == 1)
                {
                    string oString = "select ItemID,ItemName,ItemDescription,ItemImage,IsActive from tbl_Item where IsActive=1 and ItemID=" + Convert.ToString(_itemViewModel.ItemID);
                    CreateConnection();
                    SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                    _sqlConnection.Open();
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            CreateConnection();
                            string osubitemString = "select ItemID,ItemDetailsID,ItemCode,ItemSize,SalesPrice from tbl_ItemDetails where ItemID=" + Convert.ToString(oReader["ItemID"]); ;
                            SqlCommand osubitemCmd = new SqlCommand(osubitemString, _sqlConnection);
                            _sqlConnection.Open();
                            List<ItemDetailsViewModel> itemsDetailsList = new List<ItemDetailsViewModel>();
                            using (SqlDataReader osubitemReader = osubitemCmd.ExecuteReader())
                            {
                                while (osubitemReader.Read())
                                {
                                    ItemDetailsViewModel _itemsDetailsviewmodel = new ItemDetailsViewModel();
                                    _itemsDetailsviewmodel.ItemDetailsID = Convert.ToInt32(osubitemReader["ItemDetailsID"]);
                                    _itemsDetailsviewmodel.ItemID = Convert.ToInt32(osubitemReader["ItemID"]);
                                    _itemsDetailsviewmodel.ItemCode = Convert.ToString(osubitemReader["ItemCode"]);
                                    _itemsDetailsviewmodel.ItemSize = Convert.ToString(osubitemReader["ItemSize"]);
                                    _itemsDetailsviewmodel.SalesPrice = Convert.ToString(osubitemReader["SalesPrice"]);
                                    itemsDetailsList.Add(_itemsDetailsviewmodel);
                                }
                            }
                            ItemViewModel _itemsviewmodel = new ItemViewModel();
                            _itemsviewmodel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                            _itemsviewmodel.ItemName = oReader["ItemName"].ToString();
                            _itemsviewmodel.ItemDescription = oReader["ItemDescription"].ToString();
                            _itemsviewmodel.ItemImage = oReader["ItemImage"].ToString();
                            _itemsviewmodel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                            _itemsviewmodel.SubitemDetails = itemsDetailsList;
                            itemsList.Add(_itemsviewmodel);
                        }
                        _sqlConnection.Close();
                    }
                }
                // int ItemID = _itemViewModel.ItemID;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);                
            }
            return itemsList;
        }

        //public bool saveItems(ItemDetailsViewModel ItemDetailsViewModel)
        //{
        //    try
        //    {

        //        tbl_ItemDetails _itemDetailViewModel = new tbl_ItemDetails();
        //        _itemDetailViewModel.ItemID = ItemDetailsViewModel.ItemID;
        //        _itemDetailViewModel.ItemCode = ItemDetailsViewModel.ItemCode;
        //        _itemDetailViewModel.ItemSize = ItemDetailsViewModel.ItemSize;
        //        _itemDetailViewModel.IsActive = ItemDetailsViewModel.IsActive;
        //        _itemDetailViewModel.CreatedBy = ItemDetailsViewModel.CreatedBy;
        //        _itemDetailViewModel.CreatedOn = ItemDetailsViewModel.CreatedOn;
        //        _itemDetailViewModel.LastEditBy = ItemDetailsViewModel.LastEditBy;
        //        _itemDetailViewModel.LastEditOn = ItemDetailsViewModel.LastEditOn;
        //        _itemDetailViewModel.tbl_ItemDetails.Add(_itemDetailViewModel);
        //        var data = _foodcourtEntities.SaveChanges();
        //        return _isItemsSaved;
        //    }
        //    catch (Exception ex)
        //    {
        //        //log4net.ThreadContext.Properties["district"] = _districtID;
        //        //log4net.ThreadContext.Properties["userid"] = _userid;
        //        //_iloggingservice.Error("Error while saving school dates.", ex);
        //        return false;
        //    }
        //}




        //Get Categories

        public IEnumerable<CategoryViewModel> RetriveCategories(object _SectionID)
        {
            IEnumerable<CategoryViewModel> CategoryList;
            int SectionId = Convert.ToInt32(_SectionID);
            try
            {
                CategoryList = _foodcourtEntities.tbl_Category.Where(b => b.SectionID == SectionId)
                .Select(a =>
                   new CategoryViewModel
                   {
                       CategoryID=a.CategoryID,
                       CategoryName=a.CategoryName,
                       CreatedBy = a.CreatedBy,
                       CreatedOn = a.CreatedOn,
                       LastEditBy = a.LastEditBy,
                       LastEditOn = a.LastEditOn
                   });
                return CategoryList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }


        //GetItems
        public IEnumerable<ItemViewModel> RetriveItems(object _CategoryID)
        {
            List<ItemViewModel> itemsList=new List<ItemViewModel>();
            int categoryID = Convert.ToInt32(_CategoryID);
            try
            {
                //itemsList = _foodcourtEntities.tbl_Item.Where(b => b.CategoryID == categoryID)
                //.Select(a =>
                //   new ItemViewModel
                //   {   
                //       ItemID = a.ItemID,
                //       CategoryID = a.CategoryID,
                //       ItemName = a.ItemName,
                //       IsActive = a.IsActive,
                //       SubitemDetails = GetItemsDetailsByItemID(a.ItemID)
                       
                //   });
                string oString = "select ItemID,ItemName,ItemDescription,ItemImage,IsActive from tbl_Item where IsActive=1 and CategoryID=" + Convert.ToString(categoryID);
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        CreateConnection();
                        string osubitemString = "select ItemID,ItemDetailsID,ItemCode,ItemSize,SalesPrice from tbl_ItemDetails where ItemID=" + Convert.ToString(oReader["ItemID"]);
                        SqlCommand osubitemCmd = new SqlCommand(osubitemString, _sqlConnection);
                        _sqlConnection.Open();
                        List<ItemDetailsViewModel> itemsDetailsList=new List<ItemDetailsViewModel>();
                        using (SqlDataReader osubitemReader = osubitemCmd.ExecuteReader())
                        {
                            while (osubitemReader.Read())
                            {
                                ItemDetailsViewModel _itemsDetailsviewmodel = new ItemDetailsViewModel();
                                _itemsDetailsviewmodel.ItemDetailsID=Convert.ToInt32(osubitemReader["ItemDetailsID"]);
                                _itemsDetailsviewmodel.ItemID=Convert.ToInt32(osubitemReader["ItemID"]);
                                _itemsDetailsviewmodel.ItemCode=Convert.ToString(osubitemReader["ItemCode"]);
                                _itemsDetailsviewmodel.ItemSize=Convert.ToString(osubitemReader["ItemSize"]);
                                _itemsDetailsviewmodel.SalesPrice=Convert.ToString(osubitemReader["SalesPrice"]);
                                itemsDetailsList.Add(_itemsDetailsviewmodel);
                            }
                        }
                        ItemViewModel _itemsviewmodel = new ItemViewModel();
                        _itemsviewmodel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                        _itemsviewmodel.ItemName = oReader["ItemName"].ToString();
                        _itemsviewmodel.ItemDescription = oReader["ItemDescription"].ToString(); 
                        _itemsviewmodel.ItemImage = oReader["ItemImage"].ToString();
                        _itemsviewmodel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _itemsviewmodel.SubitemDetails=itemsDetailsList;
                        itemsList.Add(_itemsviewmodel);
                       
                       
                    }

                    _sqlConnection.Close();
                }

                //itemsList=
                //foreach (ItemViewModel v in itemsList)
                //{
                //    GetItemsDetailsByItemID(v.ItemID);
                //}
                return itemsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        public IEnumerable<ItemViewModel> ReriveItemGid(object _CategoryID)
        {
            List<ItemViewModel> itemsList = new List<ItemViewModel>();
            int categoryID = Convert.ToInt32(_CategoryID);
            try
            {
                //itemsList = _foodcourtEntities.tbl_Item.Where(b => b.CategoryID == categoryID)
                //.Select(a =>
                //   new ItemViewModel
                //   {   
                //       ItemID = a.ItemID,
                //       CategoryID = a.CategoryID,
                //       ItemName = a.ItemName,
                //       IsActive = a.IsActive,
                //       SubitemDetails = GetItemsDetailsByItemID(a.ItemID)

                //   });
                string oString = "select ItemID,ItemName,ItemDescription,ItemImage,IsActive from tbl_Item where IsActive=1 and ItemID=" + Convert.ToString(categoryID);
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        CreateConnection();
                        string osubitemString = "select ItemID,ItemDetailsID,ItemCode,ItemSize,SalesPrice from tbl_ItemDetails where ItemID=" + Convert.ToString(oReader["ItemID"]);
                        SqlCommand osubitemCmd = new SqlCommand(osubitemString, _sqlConnection);
                        _sqlConnection.Open();
                        List<ItemDetailsViewModel> itemsDetailsList = new List<ItemDetailsViewModel>();
                        using (SqlDataReader osubitemReader = osubitemCmd.ExecuteReader())
                        {
                            while (osubitemReader.Read())
                            {
                                ItemDetailsViewModel _itemsDetailsviewmodel = new ItemDetailsViewModel();
                                _itemsDetailsviewmodel.ItemDetailsID = Convert.ToInt32(osubitemReader["ItemDetailsID"]);
                                _itemsDetailsviewmodel.ItemID = Convert.ToInt32(osubitemReader["ItemID"]);
                                _itemsDetailsviewmodel.ItemCode = Convert.ToString(osubitemReader["ItemCode"]);
                                _itemsDetailsviewmodel.ItemSize = Convert.ToString(osubitemReader["ItemSize"]);
                                _itemsDetailsviewmodel.SalesPrice = Convert.ToString(osubitemReader["SalesPrice"]);
                                itemsDetailsList.Add(_itemsDetailsviewmodel);
                            }
                        }
                        ItemViewModel _itemsviewmodel = new ItemViewModel();
                        _itemsviewmodel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                        _itemsviewmodel.ItemName = oReader["ItemName"].ToString();
                        _itemsviewmodel.ItemDescription = oReader["ItemDescription"].ToString();
                        _itemsviewmodel.ItemImage = oReader["ItemImage"].ToString();
                        _itemsviewmodel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _itemsviewmodel.SubitemDetails = itemsDetailsList;
                        itemsList.Add(_itemsviewmodel);

                    }

                    _sqlConnection.Close();
                }

                //itemsList=
                //foreach (ItemViewModel v in itemsList)
                //{
                //    GetItemsDetailsByItemID(v.ItemID);
                //}
                return itemsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        public IEnumerable<ItemViewModel> ReriveSubItemGid(object _ItemDetailID)
        {
            List<ItemViewModel> itemsList = new List<ItemViewModel>();
            int ItemDetailsID = Convert.ToInt32(_ItemDetailID);
            try
            {
                //itemsList = _foodcourtEntities.tbl_Item.Where(b => b.CategoryID == categoryID)
                //.Select(a =>
                //   new ItemViewModel
                //   {   
                //       ItemID = a.ItemID,
                //       CategoryID = a.CategoryID,
                //       ItemName = a.ItemName,
                //       IsActive = a.IsActive,
                //       SubitemDetails = GetItemsDetailsByItemID(a.ItemID)

                //   });
                string oString = "select ItemID,ItemDetailsID,ItemCode,ItemSize,SalesPrice from tbl_ItemDetails where ItemDetailsID=" + Convert.ToString(ItemDetailsID);      
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        CreateConnection();
                        string osubitemString = "select ItemID,ItemName,ItemDescription,ItemImage,IsActive from tbl_Item where IsActive=1 And ItemID=" + Convert.ToString(oReader["ItemID"]);
                        SqlCommand osubitemCmd = new SqlCommand(osubitemString, _sqlConnection);
                        _sqlConnection.Open();
                        List<ItemDetailsViewModel> itemsDetailsList = new List<ItemDetailsViewModel>();
                        using (SqlDataReader osubitemReader = osubitemCmd.ExecuteReader())
                        {
                            while (osubitemReader.Read())
                            {
                                ItemViewModel _itemsviewmodel = new ItemViewModel();
                                _itemsviewmodel.ItemID = Convert.ToInt32(osubitemReader["ItemID"]);
                                _itemsviewmodel.ItemName = osubitemReader["ItemName"].ToString();
                                _itemsviewmodel.ItemDescription = osubitemReader["ItemDescription"].ToString();
                                _itemsviewmodel.ItemImage = osubitemReader["ItemImage"].ToString();
                                _itemsviewmodel.IsActive = Convert.ToBoolean(osubitemReader["IsActive"]);
                                _itemsviewmodel.SubitemDetails = itemsDetailsList;
                                //_itemsviewmodel.SubitemDetails[0].ItemCode = itemsDetailsList[0].ItemCode;
                                //_itemsviewmodel.SubitemDetails[0].ItemSize = itemsDetailsList[0].ItemSize;
                                itemsList.Add(_itemsviewmodel);
                            }
                        }

                        ItemDetailsViewModel _itemsDetailsviewmodel = new ItemDetailsViewModel();
                        _itemsDetailsviewmodel.ItemDetailsID = Convert.ToInt32(oReader["ItemDetailsID"]);
                        _itemsDetailsviewmodel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                        _itemsDetailsviewmodel.ItemCode = Convert.ToString(oReader["ItemCode"]);
                        _itemsDetailsviewmodel.ItemSize = Convert.ToString(oReader["ItemSize"]);
                        _itemsDetailsviewmodel.SalesPrice = Convert.ToString(oReader["SalesPrice"]);
                        itemsDetailsList.Add(_itemsDetailsviewmodel);
                     
                    }

                    _sqlConnection.Close();
                }

                //itemsList=
                //foreach (ItemViewModel v in itemsList)
                //{
                //    GetItemsDetailsByItemID(v.ItemID);
                //}
                return itemsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }
        }

        public IEnumerable<ItemViewModel> RetriveItems1(object _CategoryID)
        {
            List<ItemViewModel> itemsList = new List<ItemViewModel>();
            int categoryID = Convert.ToInt32(_CategoryID);
            try
            {
                //itemsList = _foodcourtEntities.tbl_Item.Where(b => b.CategoryID == categoryID)
                //.Select(a =>
                //   new ItemViewModel
                //   {   
                //       ItemID = a.ItemID,
                //       CategoryID = a.CategoryID,
                //       ItemName = a.ItemName,
                //       IsActive = a.IsActive,
                //       SubitemDetails = GetItemsDetailsByItemID(a.ItemID)

                //   });
                string oString = "select ItemID,ItemName,ItemDescription,ItemImage,IsActive from tbl_Item where IsActive=1 and CategoryID=" + Convert.ToString(categoryID);
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        CreateConnection();
                        string osubitemString = "select ItemID,ItemDetailsID,ItemCode,ItemSize,SalesPrice from tbl_ItemDetails where ItemID=" + Convert.ToString(oReader["ItemID"]); ;
                        SqlCommand osubitemCmd = new SqlCommand(osubitemString, _sqlConnection);
                        _sqlConnection.Open();
                        List<ItemDetailsViewModel> itemsDetailsList = new List<ItemDetailsViewModel>();
                        using (SqlDataReader osubitemReader = osubitemCmd.ExecuteReader())
                        {
                            while (osubitemReader.Read())
                            {
                                ItemDetailsViewModel _itemsDetailsviewmodel = new ItemDetailsViewModel();
                                _itemsDetailsviewmodel.ItemDetailsID = Convert.ToInt32(osubitemReader["ItemDetailsID"]);
                                _itemsDetailsviewmodel.ItemID = Convert.ToInt32(osubitemReader["ItemID"]);
                                _itemsDetailsviewmodel.ItemCode = Convert.ToString(osubitemReader["ItemCode"]);
                                _itemsDetailsviewmodel.ItemSize = Convert.ToString(osubitemReader["ItemSize"]);
                                _itemsDetailsviewmodel.SalesPrice = Convert.ToString(osubitemReader["SalesPrice"]);
                                itemsDetailsList.Add(_itemsDetailsviewmodel);
                            }
                        }
                        ItemViewModel _itemsviewmodel = new ItemViewModel();
                        _itemsviewmodel.ItemID = Convert.ToInt32(oReader["ItemID"]);
                        _itemsviewmodel.ItemName = oReader["ItemName"].ToString();
                        _itemsviewmodel.ItemDescription = oReader["ItemDescription"].ToString();
                        _itemsviewmodel.ItemImage = oReader["ItemImage"].ToString();
                        _itemsviewmodel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _itemsviewmodel.SubitemDetails = itemsDetailsList;
                     
                        itemsList.Add(_itemsviewmodel);
                    }

                    _sqlConnection.Close();
                }

                //itemsList=
                //foreach (ItemViewModel v in itemsList)
                //{
                //    GetItemsDetailsByItemID(v.ItemID);
                //}
                return itemsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        //GetItemsDetailsByItemID
        public IEnumerable<ItemDetailsViewModel> GetItemsDetailsByItemID(object _ItemID)
        {
            IEnumerable<ItemDetailsViewModel> itemsList;
            int itemID = Convert.ToInt32(_ItemID);
            try
            {
                itemsList = _foodcourtEntities.tbl_ItemDetails.Where(b => b.ItemID == itemID)
                .Select(a =>
                   new ItemDetailsViewModel
                   {
                       ItemID = a.ItemID,
                       ItemSize = a.ItemSize,
                       SalesPrice = a.SalesPrice,
                       IsActive = a.IsActive
                   });
                return itemsList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        public IEnumerable<ItemDetailsViewModel> editSubItems1(object _ItemCode)
        {
            IEnumerable<ItemDetailsViewModel> itemsListCode;
            int itemID = Convert.ToInt32(_ItemCode);
            try
            {
                itemsListCode = _foodcourtEntities.tbl_ItemDetails.Where(b => b.ItemDetailsID == itemID)
                .Select(a =>
                   new ItemDetailsViewModel
                   { 
                       
                       ItemID = a.ItemID,
                       ItemCode=a.ItemCode,
                       ItemSize = a.ItemSize,
                       SalesPrice = a.SalesPrice,
                       IsActive = a.IsActive,
                       UOM=a.UOM,
                       StackUOM=a.StackUOM,
                       TaxPrice=a.TaxPrice,
                       ItemDetailsID=a.ItemDetailsID
                       
                   });
                return itemsListCode;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        public IEnumerable<ItemViewModel> editItems1(object _ItemCode)
        {
            IEnumerable<ItemViewModel> itemsListCode;
            int itemID = Convert.ToInt32(_ItemCode);
            try
            {
                itemsListCode = _foodcourtEntities.tbl_Item.Where(b => b.ItemID == itemID)
                .Select(a =>
                   new ItemViewModel
                   {

                       ItemID = a.ItemID,
                       ItemName=a.ItemName,
                       ItemDescription=a.ItemDescription

                   });
                return itemsListCode;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }
        //delete SubItemDetails
        public bool removesubitemDetails(ItemDetailsViewModel _ItemDetailsViewModel)
        {
            try
            {
                tbl_ItemDetails _itemdetailviewmodel = new tbl_ItemDetails();
                _itemdetailviewmodel = (from a in _foodcourtEntities.tbl_ItemDetails where a.ItemDetailsID == _ItemDetailsViewModel.ItemDetailsID select a).FirstOrDefault();
                _itemdetailviewmodel.IsActive = _ItemDetailsViewModel.IsActive;
                _itemdetailviewmodel.LastEditBy = _ItemDetailsViewModel.LastEditBy;
                _itemdetailviewmodel.LastEditOn = _ItemDetailsViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupcatgorySaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        public bool removeitemDetails(ItemViewModel _ItemViewModel)
        {
            try
            {
                tbl_Item _itemviewmodel = new tbl_Item();
                _itemviewmodel = (from a in _foodcourtEntities.tbl_Item where a.ItemID == _ItemViewModel.ItemID select a).FirstOrDefault();
                _itemviewmodel.IsActive = _ItemViewModel.IsActive;
                _itemviewmodel.LastEditBy = _ItemViewModel.LastEditBy;
                _itemviewmodel.LastEditOn = _ItemViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupcatgorySaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        public bool UpdateItems(ItemViewModel _objookUpCategoryViewModel)
        {

            try
            {
                tbl_Item _lookUpCategoryViewModel = new tbl_Item();
                _lookUpCategoryViewModel = (from a in _foodcourtEntities.tbl_Item where a.ItemID == _objookUpCategoryViewModel.ItemID select a).FirstOrDefault();
                _lookUpCategoryViewModel.ItemName = _objookUpCategoryViewModel.ItemName;
                _lookUpCategoryViewModel.ItemDescription = _objookUpCategoryViewModel.ItemDescription;
                //_lookUpCategoryViewModel.LookupCategoryDescription = _objookUpCategoryViewModel.LookupCategoryDescription;
              //  _lookUpCategoryViewModel.IsActive = _objookUpCategoryViewModel.IsActive;
                // _lookUpCategoryViewModel.CreatedBy = _objookUpCategoryViewModel.CreatedBy;
                // _lookUpCategoryViewModel.CreatedOn = _objookUpCategoryViewModel.CreatedOn;
                _lookUpCategoryViewModel.LastEditBy = _objookUpCategoryViewModel.LastEditBy;
                _lookUpCategoryViewModel.LastEditOn = _objookUpCategoryViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupcatgorySaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }



        #region LookUpCategory
        //GET LOOKUPCATEGORY
        public IEnumerable<LookUpCategoryViewModel> GetLookUpCategory()
        {
            IEnumerable<LookUpCategoryViewModel> LookUpCategoryList;
            try
            {
                List<LookUpCategoryViewModel> _lookupcategoryViewModelList = new List<LookUpCategoryViewModel>();
                string oString = "Select * from tbl_LookupCategory where IsActive=1";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        LookUpCategoryViewModel _lookuocategoryViewModel = new LookUpCategoryViewModel();
                        _lookuocategoryViewModel.LookupCategoryID = Convert.ToInt32(oReader["LookupCategoryID"]);
                        _lookuocategoryViewModel.LookupCategoryName = oReader["LookupCategoryName"].ToString();
                        _lookuocategoryViewModel.LookupCategoryCode = oReader["LookupCategoryCode"].ToString();
                        _lookuocategoryViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _lookupcategoryViewModelList.Add(_lookuocategoryViewModel);
                    }

                    _sqlConnection.Close();
                }
                LookUpCategoryList = _lookupcategoryViewModelList;
                return LookUpCategoryList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }
        //save LOOKUPCATEGORY
        public bool saveLookupcategory(LookUpCategoryViewModel objlookUpCategoryViewModel)
        {
            try
            {

                tbl_LookupCategory _lookUpCategoryViewModel = new tbl_LookupCategory();
                _lookUpCategoryViewModel.LookupCategoryName = objlookUpCategoryViewModel.LookupCategoryName;
                _lookUpCategoryViewModel.LookupCategoryCode = objlookUpCategoryViewModel.LookupCategoryCode;
                _lookUpCategoryViewModel.LookupCategoryDescription = objlookUpCategoryViewModel.LookupCategoryDescription;
                _lookUpCategoryViewModel.IsActive = objlookUpCategoryViewModel.IsActive;
                _lookUpCategoryViewModel.CreatedBy = objlookUpCategoryViewModel.CreatedBy;
                _lookUpCategoryViewModel.CreatedOn = objlookUpCategoryViewModel.CreatedOn;
                _lookUpCategoryViewModel.LastEditBy = objlookUpCategoryViewModel.LastEditBy;
                _lookUpCategoryViewModel.LastEditOn = objlookUpCategoryViewModel.LastEditOn;
                _foodcourtEntities.tbl_LookupCategory.Add(_lookUpCategoryViewModel);
                var data = _foodcourtEntities.SaveChanges();
                return _isLookupcatgorySaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        //POST api/updateRestaurantDetails
        public bool updateLookUpCategoryDetails(LookUpCategoryViewModel _objookUpCategoryViewModel)
        {
           
            try
            {
                tbl_LookupCategory _lookUpCategoryViewModel = new tbl_LookupCategory();
                _lookUpCategoryViewModel = (from a in _foodcourtEntities.tbl_LookupCategory where a.LookupCategoryID == _objookUpCategoryViewModel.LookupCategoryID select a).FirstOrDefault();
                _lookUpCategoryViewModel.LookupCategoryName = _objookUpCategoryViewModel.LookupCategoryName;
                _lookUpCategoryViewModel.LookupCategoryCode = _objookUpCategoryViewModel.LookupCategoryCode;
                //_lookUpCategoryViewModel.LookupCategoryDescription = _objookUpCategoryViewModel.LookupCategoryDescription;
                _lookUpCategoryViewModel.IsActive = _objookUpCategoryViewModel.IsActive;
               // _lookUpCategoryViewModel.CreatedBy = _objookUpCategoryViewModel.CreatedBy;
               // _lookUpCategoryViewModel.CreatedOn = _objookUpCategoryViewModel.CreatedOn;
                _lookUpCategoryViewModel.LastEditBy = _objookUpCategoryViewModel.LastEditBy;
                _lookUpCategoryViewModel.LastEditOn = _objookUpCategoryViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupcatgorySaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        //POST api/deleteRestaurantDetails
        public bool removeLookupcategories(LookUpCategoryViewModel _objookUpCategoryViewModel)
        {
            try
            {
                tbl_LookupCategory _lookUpCategoryViewModel = new tbl_LookupCategory();
                _lookUpCategoryViewModel = (from a in _foodcourtEntities.tbl_LookupCategory where a.LookupCategoryID == _objookUpCategoryViewModel.LookupCategoryID select a).FirstOrDefault();
                _lookUpCategoryViewModel.IsActive = _objookUpCategoryViewModel.IsActive;
                _lookUpCategoryViewModel.LastEditBy = _objookUpCategoryViewModel.LastEditBy;
                _lookUpCategoryViewModel.LastEditOn = _objookUpCategoryViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupcatgorySaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        [System.Web.Http.HttpGet]

         public IEnumerable<LookUpCategoryViewModel> RetriveCategoryCode(string CategoryCode)
        {
            string categoryCode = Convert.ToString(CategoryCode);
            IEnumerable<LookUpCategoryViewModel> ItemsList;
            try
            {
                List<LookUpCategoryViewModel> _ItemViewModelList = new List<LookUpCategoryViewModel>();
                string oString = "select LookupCategoryCode from tbl_LookupCategory where LookupCategoryCode='"+categoryCode+"'";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        LookUpCategoryViewModel _ItemViewModel = new LookUpCategoryViewModel();
                        _ItemViewModel.LookupCategoryCode = oReader["LookupCategoryCode"].ToString();
                       // _ItemViewModel.CategoryID = Convert.ToInt32(oReader["CategoryID"]);
                       // _ItemViewModel.ItemName = oReader["ItemName"].ToString();
                        //_ItemViewModel.ItemDescription = oReader["ItemDescription"].ToString();
                        //_ItemViewModel.ItemImage = oReader["ItemImage"].ToString();
                        //ItemViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        //_ItemViewModel.CreatedBy = Convert.ToInt32(oReader["CreatedBy"]);
                       // _ItemViewModel.LastEditBy = Convert.ToInt32(oReader["LastEditBy"]);
                        _ItemViewModelList.Add(_ItemViewModel);
                    }

                    _sqlConnection.Close();
                }
                //if (_ItemViewModelList.Count==1)
                //{
                //    ItemsList = _ItemViewModelList;
                //    return ItemsList;

                //}
                ItemsList = _ItemViewModelList;
                return ItemsList;
               
              
                
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }
        }

        //LookUpCode Checking whether exists or not RetriveCategoryCode
        //public IEnumerable<LookUpCategoryViewModel> RetriveCategoryCode(string CategoryCode)
        //{
        //    string categoryCode = Convert.ToString(CategoryCode);
        //    IEnumerable<LookUpCategoryViewModel> CategorycodeList;
        //   // string CategorycodeList;
        //    try
        //    {
        //        CategorycodeList = _foodcourtEntities.tbl_LookupCategory.Where(b => b.LookupCategoryCode == categoryCode)
        //        .Select(a =>
        //           new LookUpCategoryViewModel
        //           {
        //               LookupCategoryCode = a.LookupCategoryCode

        //           });
        //        return CategorycodeList;  
                
        //    }
                
        //    catch (Exception ex)
        //    {
        //        //log4net.ThreadContext.Properties["district"] = _districtID;
        //        //log4net.ThreadContext.Properties["userid"] = _userid;
        //        //_iloggingservice.Error("Error while retrieving school dates.", ex);
        //        throw ex;
        //    }

        //}

        #endregion LookUpCategory

        #region LookUp
        //save lookup
        public bool saveLookup(LookUpViewModel objlookupviewmodel)
        {
            try
            {

                tbl_Lookup _lookUpViewModel = new tbl_Lookup();
                _lookUpViewModel.LookupCategoryID = objlookupviewmodel.LookupCategoryID;
                _lookUpViewModel.LookupName = objlookupviewmodel.LookupName;
                _lookUpViewModel.LookupCode = objlookupviewmodel.LookupCode;
                _lookUpViewModel.LookupDescription = objlookupviewmodel.LookupDescription;
                _lookUpViewModel.IsActive = objlookupviewmodel.IsActive;
                _lookUpViewModel.CreatedBy = objlookupviewmodel.CreatedBy;
                _lookUpViewModel.CreatedOn = objlookupviewmodel.CreatedOn;
                _lookUpViewModel.LastEditBy = objlookupviewmodel.LastEditBy;
                _lookUpViewModel.LastEditOn = objlookupviewmodel.LastEditOn;
                _foodcourtEntities.tbl_Lookup.Add(_lookUpViewModel);
                var data = _foodcourtEntities.SaveChanges();
                return _isLookupSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        //GET LOOKUP
        public IEnumerable<LookUpViewModel> GetLookUp()
        {
            IEnumerable<LookUpViewModel> LookUpList;
            try
            {
                List<LookUpViewModel> _lookupViewModelList = new List<LookUpViewModel>();
                string oString = "Select * from tbl_Lookup where IsActive=1";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        LookUpViewModel _lookupViewModel = new LookUpViewModel();
                        _lookupViewModel.LookupID = Convert.ToInt32(oReader["LookupID"]);
                        _lookupViewModel.LookupCategoryID = Convert.ToInt32(oReader["LookupCategoryID"]);
                        _lookupViewModel.LookupName = oReader["LookupName"].ToString();
                        _lookupViewModel.LookupCode = oReader["LookupCode"].ToString();
                        _lookupViewModel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _lookupViewModelList.Add(_lookupViewModel);
                    }

                    _sqlConnection.Close();
                }
                LookUpList = _lookupViewModelList;
                return LookUpList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }
        //Retrive Grid data by categoryid
        public IEnumerable<LookUpViewModel> RetriveLookupdata(object _lookupCategorytID)
        {
            IEnumerable<LookUpViewModel> LookupdataList;
            int lookupcategoryID = Convert.ToInt32(_lookupCategorytID);
            try
            {
                LookupdataList = _foodcourtEntities.tbl_Lookup.Where(b => b.LookupCategoryID == lookupcategoryID && b.IsActive==true)
                .Select(a =>
                   new LookUpViewModel
                   {
                       LookupID = a.LookupID,
                       LookupCategoryID = a.LookupCategoryID,
                       LookupName = a.LookupName,
                       LookupCode = a.LookupCode,
                       LookupDescription=a.LookupDescription,
                       IsActive = a.IsActive,
                       CreatedBy = a.CreatedBy,
                       CreatedOn = a.CreatedOn,
                       LastEditBy = a.LastEditBy,
                       LastEditOn = a.LastEditOn
                   });
                return LookupdataList;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

       // POST api/updateLookupDetails
        public bool updateLookUpDetails(LookUpViewModel _objookUpViewModel)
         {

            try
            {
                tbl_Lookup _lookUpViewModel = new tbl_Lookup();
                _lookUpViewModel = (from a in _foodcourtEntities.tbl_Lookup where a.LookupID == _objookUpViewModel.LookupID select a).FirstOrDefault();
                _lookUpViewModel.LookupCategoryID = _objookUpViewModel.LookupCategoryID;
                _lookUpViewModel.LookupName = _objookUpViewModel.LookupName;
                _lookUpViewModel.LookupCode = _objookUpViewModel.LookupCode;
                _lookUpViewModel.LookupDescription = _objookUpViewModel.LookupDescription;
                _lookUpViewModel.IsActive = _objookUpViewModel.IsActive;
                // _lookUpCategoryViewModel.CreatedBy = _objookUpCategoryViewModel.CreatedBy;
                // _lookUpCategoryViewModel.CreatedOn = _objookUpCategoryViewModel.CreatedOn;
                _lookUpViewModel.LastEditBy = _objookUpViewModel.LastEditBy;
                _lookUpViewModel.LastEditOn = _objookUpViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }
        //remove look up data by lookupid
        public bool removeLookup(LookUpViewModel _objlookup)
        {
            try
            {
                tbl_Lookup _lookUpViewModel = new tbl_Lookup();
                _lookUpViewModel = (from a in _foodcourtEntities.tbl_Lookup where a.LookupID == _objlookup.LookupID select a).FirstOrDefault();
                _lookUpViewModel.IsActive = _objlookup.IsActive;
                _lookUpViewModel.LastEditBy = _objlookup.LastEditBy;
                _lookUpViewModel.LastEditOn = _objlookup.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isLookupSaved;
            }
            catch (Exception ex)
            {
             
                return false;
            }
        }

        #endregion LookUp

        #region Customer

        public IEnumerable<CustomerViewModel> GetCustomers()
        {
            IEnumerable<CustomerViewModel> customerlist;
            try
            {
                List<CustomerViewModel> _customerViewModelList = new List<CustomerViewModel>();
                string oString = "Select * from tbl_Customer where IsActive=1";
                CreateConnection();
                SqlCommand oCmd = new SqlCommand(oString, _sqlConnection);
                _sqlConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        CustomerViewModel _customerviewmodel = new CustomerViewModel();
                        _customerviewmodel.CustomerID = Convert.ToInt32(oReader["CustomerID"]);
                        _customerviewmodel.CustomerName = oReader["CustomerName"].ToString();
                        _customerviewmodel.Email = oReader["Email"].ToString();
                        _customerviewmodel.Phone = oReader["Phone"].ToString();
                        _customerviewmodel.Address1 = oReader["Address1"].ToString();
                        _customerviewmodel.Address2 = oReader["Address2"].ToString();
                        _customerviewmodel.City = oReader["City"].ToString();
                        _customerviewmodel.State = oReader["State"].ToString();
                        _customerviewmodel.Country = oReader["Country"].ToString();
                        _customerviewmodel.ZIPCode = oReader["ZIPCode"].ToString();
                        _customerviewmodel.IsActive = Convert.ToBoolean(oReader["IsActive"]);
                        _customerviewmodel.CreatedBy = Convert.ToInt32(oReader["CreatedBy"]);
                        _customerviewmodel.LastEditBy = Convert.ToInt32(oReader["LastEditBy"]);
                        _customerViewModelList.Add(_customerviewmodel);
                    }

                    _sqlConnection.Close();
                }
                customerlist = _customerViewModelList;
                return customerlist;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while retrieving school dates.", ex);
                throw ex;
            }

        }

        public bool UpdateCustomerDetails(CustomerViewModel _CustomerViewModelViewModel)
        {
            try
            {
                tbl_Customer _customerViewModel = new tbl_Customer();
                _customerViewModel = (from a in _foodcourtEntities.tbl_Customer where a.CustomerID == _CustomerViewModelViewModel.CustomerID select a).FirstOrDefault();
                _customerViewModel.CustomerName = _CustomerViewModelViewModel.CustomerName;
                _customerViewModel.Email = _CustomerViewModelViewModel.Email;
                _customerViewModel.Address1 = _CustomerViewModelViewModel.Address1;
               // _customerViewModel.Address2 = _CustomerViewModelViewModel.Address2;
                _customerViewModel.Phone = _CustomerViewModelViewModel.Phone;
               // _customerViewModel.State = _CustomerViewModelViewModel.State;
                _customerViewModel.City = _CustomerViewModelViewModel.City;
                _customerViewModel.Country = _CustomerViewModelViewModel.Country;
                //_customerViewModel.ZIPCode = _CustomerViewModelViewModel.ZIPCode;
                _customerViewModel.IsActive = _CustomerViewModelViewModel.IsActive;
                _customerViewModel.CreatedBy = _CustomerViewModelViewModel.CreatedBy;
                _customerViewModel.CreatedOn = _CustomerViewModelViewModel.CreatedOn;
                _customerViewModel.LastEditBy = _CustomerViewModelViewModel.LastEditBy;
                _customerViewModel.LastEditOn = _CustomerViewModelViewModel.LastEditOn;
                _foodcourtEntities.SaveChanges();
                return _isCustomerDetailsSaved;
            }
            catch (Exception ex)
            {
                //log4net.ThreadContext.Properties["district"] = _districtID;
                //log4net.ThreadContext.Properties["userid"] = _userid;
                //_iloggingservice.Error("Error while saving school dates.", ex);
                return false;
            }
        }

        #endregion Customer






    }
}
